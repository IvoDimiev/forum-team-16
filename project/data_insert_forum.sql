-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table forum.categories: ~8 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `category_name`) VALUES
	(2, 'Active Learning'),
	(3, 'Constructive Feedback'),
	(6, 'Emotional Intelligence'),
	(5, 'Mental Toughness'),
	(1, 'off-topic'),
	(7, 'Social Intelligence'),
	(4, 'Teamwork and Collaboration'),
	(8, 'Uncategorized');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping data for table forum.comments: ~8 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`comment_id`, `post_id`, `content`, `creation_date`, `user_id`) VALUES
	(52, 55, 'They listen to their mentors! They are teachable!', '2022-03-28 11:01:37', 16),
	(53, 61, 'I am a very emotional person! I do not like cold people....', '2022-03-28 11:02:16', 16),
	(54, 65, 'Javascript is way better... Do not use java at all...', '2022-03-28 11:03:13', 16),
	(55, 66, 'I bet he uses a bicycle to go to work :)', '2022-03-28 11:07:02', 15),
	(56, 56, 'Wow!\r\nWhat a great post! Could you link me the article?\r\nThank you. ', '2022-03-28 11:08:07', 15),
	(57, 57, 'Wait, are you just copy pasting information? You did not answer my post for 10 days, even after you told us to be active, and now you just copy paste?', '2022-03-28 11:10:16', 15),
	(58, 59, 'I drive an AUDI A8 D3 4.2TDI. Do not buy it! Costs me thousand of dollars to maintain..', '2022-03-28 11:12:40', 15),
	(59, 62, 'Yes, Telerik have a great soft skills section, but I can not link them here....', '2022-03-28 11:14:06', 17);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping data for table forum.comments_likes: ~10 rows (approximately)
/*!40000 ALTER TABLE `comments_likes` DISABLE KEYS */;
INSERT INTO `comments_likes` (`comment_id`, `user_id`) VALUES
	(52, 16),
	(57, 15),
	(52, 15),
	(55, 15),
	(58, 15),
	(58, 17),
	(59, 17),
	(55, 2),
	(58, 2),
	(56, 2);
/*!40000 ALTER TABLE `comments_likes` ENABLE KEYS */;

-- Dumping data for table forum.images: ~0 rows (approximately)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;

-- Dumping data for table forum.phone_numbers: ~0 rows (approximately)
/*!40000 ALTER TABLE `phone_numbers` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_numbers` ENABLE KEYS */;

-- Dumping data for table forum.posts: ~12 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`post_id`, `user_id`, `category_id`, `title`, `content`, `creation_date`) VALUES
	(54, 2, 2, 'Mentorship - How to Give and Get the best out of it?', 'Mentorship requires intentional investments of time and energy - you get what you put in.', '2022-03-28 10:13:20'),
	(55, 2, 2, 'What great mentees do?', 'Great mentees establish the gramework of the relationship. They work at the relationship. They are prepared with specific questions, areas, for feedback, ', '2022-03-28 10:21:06'),
	(56, 2, 3, 'Constructive Feedback', 'Constructive feedback is the type of feedback aimed at achieving a positive outcome by providing someone with comments, advice, or suggestions that are useful for their work or their future. The outcome can be faster processes, improving behaviors, identifying weaknesses, or providing new perspectives.', '2022-03-28 10:26:31'),
	(57, 2, 3, 'Purpose of Constructive Feedback', 'For example, constructive feedback can:\r\n\r\nImprove employee morale\r\nReduce confusion regarding expectations and current performance\r\nProvide a new perspective and give valuable insight to the person receiving feedback\r\nPositively impact an individual’s behavior', '2022-03-28 10:27:07'),
	(59, 14, 1, 'Fast vehicles of all kind', 'Hello guys, what cars do you drive??', '2022-03-28 10:35:48'),
	(60, 14, 5, 'Mental toughness', 'Mental toughness is "Having the natural or developed psychological edge that enables you to: generally, cope better than your opponents with the many demands (competition, training, lifestyle) that sport places on a performer. Are you mentally tough??', '2022-03-28 10:37:12'),
	(61, 14, 6, 'Emotional intelligence is a must!', 'Emotional intelligence (EI) is most often defined as the ability to perceive, use, understand, manage, and handle emotions. Are you emotinally intelligent at work?', '2022-03-28 10:38:36'),
	(62, 14, 7, 'Social intelligence is a must!', 'Social intelligence is the capacity to communicate and form relationships with empathy and assertiveness. It comes from knowing yourself and exercising proper emotional management. We can say it is closely linked to emotional intelligence, but it is not exactly the same thing. I hope you guys can share cool materials!!', '2022-03-28 10:46:19'),
	(63, 3, 4, 'Benefits of teamwork and collaboration', 'Increases productivity and efficiency, enhances social skills and communication. \r\nWhat other skills can benefit from a good team?', '2022-03-28 10:52:28'),
	(65, 3, 8, 'Java  (programming language)', 'Java is a very interesting language, is it still alive??', '2022-03-28 10:54:30'),
	(66, 16, 7, 'I have problems with my coworker', 'I do not come along with my coworker, because he snitched me out to the boss?! Who does that?', '2022-03-28 10:59:56'),
	(67, 2, 2, 'Active learning as a team is great', 'I love solving problems with my team, because we learn more that way', '2022-03-28 11:46:04');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping data for table forum.posts_likes: ~17 rows (approximately)
/*!40000 ALTER TABLE `posts_likes` DISABLE KEYS */;
INSERT INTO `posts_likes` (`post_id`, `user_id`) VALUES
	(66, 16),
	(65, 16),
	(63, 16),
	(62, 16),
	(61, 16),
	(60, 16),
	(55, 16),
	(56, 16),
	(57, 16),
	(56, 15),
	(57, 15),
	(66, 15),
	(63, 15),
	(60, 15),
	(66, 17),
	(66, 2),
	(67, 2);
/*!40000 ALTER TABLE `posts_likes` ENABLE KEYS */;

-- Dumping data for table forum.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role_name`) VALUES
	(2, 'admin'),
	(1, 'user');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table forum.tags: ~12 rows (approximately)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`tag_id`, `tag_name`) VALUES
	(33, 'active-learning'),
	(36, 'cars'),
	(41, 'developer'),
	(38, 'emotinally-intelligent'),
	(35, 'emotions'),
	(34, 'feedback'),
	(42, 'java'),
	(37, 'mental-toughness'),
	(32, 'mentorship'),
	(43, 'problems'),
	(39, 'social-intelligence'),
	(40, 'team');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping data for table forum.tag_post: ~18 rows (approximately)
/*!40000 ALTER TABLE `tag_post` DISABLE KEYS */;
INSERT INTO `tag_post` (`tag_id`, `post_id`) VALUES
	(32, 54),
	(33, 54),
	(32, 55),
	(33, 55),
	(34, 56),
	(34, 57),
	(36, 59),
	(37, 60),
	(38, 61),
	(39, 62),
	(40, 63),
	(41, 65),
	(42, 65),
	(43, 66),
	(40, 67),
	(33, 67),
	(43, 67),
	(34, 67);
/*!40000 ALTER TABLE `tag_post` ENABLE KEYS */;

-- Dumping data for table forum.users: ~7 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `role_id`, `email`, `password`, `username`, `first_name`, `last_name`, `creation_date`, `blocked`, `is_deleted`) VALUES
	(0, 1, 'random@gmail.com', '12345678', 'RandomIvanov', 'random', 'Ivanov', '2022-03-27 19:32:49', 0, 0),
	(2, 2, 'dimievivo@gmail.com', '12345678', 'ivo', 'Ivoo', 'Dimiev', '2022-02-23 11:17:08', 0, 0),
	(3, 2, 'sofi.stoyanova@gmail.com', '1234567890', 'js_god', 'Sofi', 'Stoyanova', '2022-02-23 11:34:42', 0, 0),
	(14, 1, 'theDoctor@gmail.com', '12345678', 'TheDoctor', 'Hristo', 'Cankov', '2022-03-28 10:00:25', 0, 0),
	(15, 1, 'gdimiev@gmail.com', '12345678', 'CryptoHater', 'Tony', 'Soprano', '2022-03-28 10:06:03', 0, 0),
	(16, 1, 'poly@gmail.com', '12345678', 'need_more_coffee', 'Paulie', 'Gualtieri', '2022-03-28 10:08:51', 0, 0),
	(17, 1, 'rose@gmail.com', '12345678', 'happee', 'Rose', 'DaVinki', '2022-03-28 10:10:20', 0, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
