package com.example.forumproject.services;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.Tag;
import com.example.forumproject.models.Tag;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.CategoryRepository;
import com.example.forumproject.repositories.contracts.CommentRepository;
import com.example.forumproject.repositories.contracts.PostRepository;
import com.example.forumproject.repositories.contracts.TagRepository;
import com.example.forumproject.services.contracts.PostService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.forumproject.Helpers.*;
import static com.example.forumproject.Helpers.createMockAdmin;

@ExtendWith(MockitoExtension.class)
public class TagServiceTests {
    @Mock
    TagRepository mockTagRepository;

    @Mock
    PostService mockPostService;

    @InjectMocks
    TagServiceImpl tagService;

    @Test
    public void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockTagRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        tagService.getAll();

        // Assert
        Mockito.verify(mockTagRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_category_when_categoryExists() {
        // Arrange
        Tag mockTag = createMockTag();
        Mockito.when(mockTagRepository.getById(mockTag.getId()))
                .thenReturn(mockTag);
        // Act
        Tag result = tagService.getById(mockTag.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockTag.getId(), result.getId()),
                () -> Assertions.assertEquals(mockTag.getTagName(), result.getTagName())
        );
    }

    @Test
    public void getByName_should_return_tag_when_tagExists() {
        // Arrange
        Tag mockTag = createMockTag();
        Mockito.when(mockTagRepository.getByName(mockTag.getTagName()))
                .thenReturn(mockTag);
        // Act
        Tag result = tagService.getByName(mockTag.getTagName());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockTag.getId(), result.getId()),
                () -> Assertions.assertEquals(mockTag.getTagName(), result.getTagName())
        );
    }

    @Test
    public void creatTag_should_throw_when_userToExecuteIsBlocked() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();
        mockUser.setBlocked(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.create(mockTag,mockUser));
    }

    @Test
    public void creatTag_should_throw_when_userToExecuteIsDeleted() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();
        mockUser.setDeleted(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.create(mockTag, mockUser));
    }

    @Test
    public void creatTag_should_throw_when_TagAlreadyExists() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> tagService.create(mockTag, mockUser));
    }

    @Test
    public void creatTag_should_callRepository_when_UserIsAdmin() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();

        Mockito.when(mockTagRepository.getByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        tagService.create(mockTag,mockUser);
        // Act, Assert
        Mockito.verify(mockTagRepository, Mockito.times(1))
                .create(mockTag);
    }

    @Test
    public void updateTag_should_throw_when_userToExecuteIsBlocked() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();
        Tag anotherMockTag = createMockTag();
        anotherMockTag.setId(12);
        anotherMockTag.setTagName("AnotherMockTag");
        mockUser.setBlocked(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.update(mockTag, mockUser));
    }

    @Test
    public void updateTag_should_throw_when_userToExecuteIsNotAdmin() {
        // Arrange
        User mockUser = createMockUser();
        Tag mockTag = createMockTag();
        Tag anotherMockTag = createMockTag();
        anotherMockTag.setId(12);
        anotherMockTag.setTagName("AnotherMockTag");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.update(mockTag, mockUser));
    }

    @Test
    public void updateTag_should_throw_when_userToExecuteIsDeleted() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();
        Tag anotherMockTag = createMockTag();
        anotherMockTag.setId(12);
        anotherMockTag.setTagName("AnotherMockTag");

        mockUser.setDeleted(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.update(mockTag, mockUser));
    }

    @Test
    public void updateTag_should_throw_when_TagAlreadyExists() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> tagService.update(mockTag, mockUser));
    }

    @Test
    public void updateTag_should_callRepository_when_UserIsAdmin() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();
        Tag anotherMockTag = createMockTag();
        anotherMockTag.setId(12);
        anotherMockTag.setTagName("AnotherMockTag2");
        Post mockPost = createMockPost();
        List<Post> posts = new ArrayList<>();
        posts.add(mockPost);

//        Mockito.when(mockPostRepository.getAll())
//                .thenReturn(posts);

        Mockito.when(mockTagRepository.getByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        tagService.update(mockTag, mockUser);

        // Act, Assert
        Mockito.verify(mockTagRepository, Mockito.times(1))
                .update(mockTag);
    } // is this test a problem?

    @Test
    public void deleteTag_should_throw_when_userToExecuteIsBlocked() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();
        mockUser.setBlocked(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.delete(mockTag.getId(), mockUser));
    }

    @Test
    public void deleteTag_should_throw_when_userToExecuteIsNotAdmin() {
        // Arrange
        User mockUser = createMockUser();
        Tag mockTag = createMockTag();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.delete(mockTag.getId(), mockUser));
    }

    @Test
    public void deleteTag_should_throw_when_userToExecuteIsDeleted() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();
        mockUser.setDeleted(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.delete(mockTag.getId(), mockUser));
    }

    @Test
    public void deleteTag_should_callRepository_when_UserIsAdmin() {
        // Arrange
        User mockUser = createMockAdmin();
        Tag mockTag = createMockTag();

        Post mockPost = createMockPost();
        List<Post> posts = new ArrayList<>();
        posts.add(mockPost);

        Mockito.when(mockTagRepository.getById(mockTag.getId()))
                .thenReturn(mockTag);

        Mockito.when(mockPostService.getAll())
                .thenReturn(posts);

        tagService.delete(mockTag.getId(), mockUser);

        // Act, Assert
        Mockito.verify(mockTagRepository, Mockito.times(1))
                .delete(mockTag);
    }
}