package com.example.forumproject.services;


import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.Category;
import com.example.forumproject.models.Comment;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.forumproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTests {

    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_category_when_categoryExists() {
        Category mockCategory = createMockCategory();
        Mockito.when(mockRepository.getById(mockCategory.getId()))
                .thenReturn(mockCategory);

        Category result = service.getById(mockCategory.getId());


        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getName(), result.getName())
        );
    }

    @Test
    public void createCategory_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();
        mockUser.setBlocked(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockCategory,mockUser));
    }

    @Test
    public void createCategory_should_throw_when_userToExecuteIsNotAdmin() {
        User mockUser = createMockUser();
        Category mockCategory = createMockCategory();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockCategory,mockUser));
    }

    @Test
    public void createCategory_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();
        mockUser.setDeleted(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockCategory,mockUser));
    }

    @Test
    public void createCategory_should_throw_when_CategoryAlreadyExists() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockCategory,mockUser));
    }

    @Test
    public void createCategory_should_callRepository_when_UserIsAdmin() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                        .thenThrow(EntityNotFoundException.class);

        service.create(mockCategory,mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockCategory);
    }

    @Test
    public void updateCategory_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();
        mockCategory.setName("AnotherMockCategory");
        mockUser.setBlocked(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockCategory, mockUser));
    }

    @Test
    public void updateCategory_should_throw_when_userToExecuteIsNotAdmin() {
        User mockUser = createMockUser();
        Category mockCategory = createMockCategory();
        mockCategory.setName("AnotherMockCategory");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockCategory, mockUser));
    }

    @Test
    public void updateCategory_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();
        mockCategory.setName("AnotherMockCategory");
        mockUser.setDeleted(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockCategory, mockUser));
    }

    @Test
    public void updateCategory_should_throw_when_CategoryAlreadyExists() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();


        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockCategory, mockUser));
    }

    @Test
    public void updateCategory_should_callRepository_when_UserIsAdmin() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();
        mockCategory.setName("AnotherMockCategory");

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        service.update(mockCategory, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCategory);
    }

    @Test
    public void deleteCategory_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();
        mockUser.setBlocked(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCategory);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockCategory.getId(), mockUser));
    }

    @Test
    public void deleteCategory_should_throw_when_userToExecuteIsNotAdmin() {
        User mockUser = createMockUser();
        Category mockCategory = createMockCategory();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCategory);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockCategory.getId(), mockUser));
    }

    @Test
    public void deleteCategory_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();
        mockUser.setDeleted(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockCategory.getId(), mockUser));
    }

    @Test
    public void deleteCategory_should_callRepository_when_UserIsAdmin() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCategory);

        service.delete(mockCategory.getId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockCategory);
    }

}
