package com.example.forumproject.services;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.PhoneNumber;
import com.example.forumproject.models.Role;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.PhoneNumberRepository;
import com.example.forumproject.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.forumproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository mockRepository;

    @Mock
    PhoneNumberRepository phoneNumberRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_User_when_userExists() {
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        User result = service.getById(mockUser.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getRole(), result.getRole()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword())
        );
    }

    @Test
    public void getByUsername_should_return_User_when_userExists() {
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);

        User result = service.getByUsername(mockUser.getUsername());


        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getRole(), result.getRole()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword())
        );
    }

    @Test
    public void create_should_throw_when_userWithSameUsernameExists() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail())).thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);



        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
    }

    @Test
    public void create_should_throw_when_userWithSameEmailExists() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
    }

    @Test
    public void create_should_callRepository_when_userWithSameEmailDoesNotExist() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByUsername(mockUser.getUsername())).thenThrow(EntityNotFoundException.class);

        service.create(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    public void createPhoneNumber_should_throw_when_userToExecuteRoleIsNotAdmin() {
        User mockUserToExecute = createMockUser();
        User mockAdminToGetPhone = createMockAdmin();
        PhoneNumber mockPhoneNumber = createMockPhoneNumber();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockAdminToGetPhone);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.createPhoneNumber(mockUserToExecute, mockAdminToGetPhone.getId(), mockPhoneNumber));
    }

    @Test
    public void createPhoneNumber_should_throw_when_userToGetPhoneRoleIsNotAdmin() {
        User mockAdminToExecute = createMockAdmin();
        User mockUserToGetPhone = createMockUser();
        PhoneNumber mockPhoneNumber = createMockPhoneNumber();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToGetPhone);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.createPhoneNumber(mockAdminToExecute, mockUserToGetPhone.getId(), mockPhoneNumber));
    }

    @Test
    public void createPhoneNumber_should_throw_when_phoneNumberAlreadyExists() {
        User mockAdminToExecute = createMockAdmin();
        User mockAdminToReceivePhone = createMockAdmin();
        PhoneNumber mockPhoneNumber = createMockPhoneNumber();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockAdminToReceivePhone);

        Mockito.when(phoneNumberRepository.getByUserId(Mockito.anyInt()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.createPhoneNumber(mockAdminToExecute, mockAdminToReceivePhone.getId(), mockPhoneNumber));
    }

    @Test
    public void createPhoneNumber_should_callRepository_when_theSamePhoneNumberDoesNotExist() {
        User mockAdminToExecute = createMockAdmin();
        User mockAdminToGetPhone = createMockAdmin();
        PhoneNumber mockPhoneNumber = createMockPhoneNumber();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockAdminToGetPhone);
        Mockito.when(phoneNumberRepository.getByUserId(Mockito.anyInt()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(phoneNumberRepository.getByPhoneNumber(mockPhoneNumber.getPhoneNumber()))
                .thenThrow(EntityNotFoundException.class);

        service.createPhoneNumber(mockAdminToExecute, mockAdminToGetPhone.getId(), mockPhoneNumber);


        Mockito.verify(phoneNumberRepository, Mockito.times(1))
                .create(mockPhoneNumber);
    }

    @Test
    public void update_should_throwException_when_userIsNotAdminOrOwner() {
        User mockInitiator = createMockUser();
        User mockUserObject = createMockUser2();


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockInitiator, mockUserObject));
    }

    @Test
    public void update_should_throwException_when_userEmailAlreadyExists() {
        User mockUserObject = createMockUser2();
        User mockInitiator = createMockAdmin();
        mockUserObject.setEmail("mock@user.com");

        Mockito.when(mockRepository.getByEmail(Mockito.anyString()))
                .thenReturn(mockInitiator);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockInitiator, mockUserObject));
    }

    @Test
    public void update_should_return_updateUser_when_userIsAdmin() {
        User mockInitiator = createMockAdmin();
        User mockUserObject = createMockUser2();

        Mockito.when(mockRepository.getByEmail(mockUserObject.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        service.update(mockInitiator, mockUserObject);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUserObject);

    }

    @Test
    public void update_should_return_update_when_userIsOwner() {
        User mockInitiator = createMockUser2();
        User mockUserToBeUpdated = mockInitiator;


        Mockito.when(mockRepository.getByEmail(mockInitiator.getEmail()))
                .thenReturn(mockInitiator);


        service.update(mockInitiator, mockUserToBeUpdated);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockInitiator);
    }

    @Test
    public void updateRole_should_return_update_when_executingUserIsAdmin() {
        User executingUser = createMockAdmin();
        User mockUserToBeUpdated = createMockUser();
        Role mockRole = createMockRole("admin");

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        service.updateRole(executingUser, mockUserToBeUpdated.getId(), mockRole);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUserToBeUpdated);
    }

    @Test
    public void updateRole_should_throw_when_executingUserIsNotAdmin() {
        User executingUser = createMockUser2();
        User mockUserToBeUpdated = createMockUser();
        Role mockRole = createMockRole("admin");

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updateRole(executingUser, mockUserToBeUpdated.getId(), mockRole));
    }

    @Test
    public void updateBlocked_should_throw_when_executingUserIsNotAdmin() {
        // Arrange
        User executingUser = createMockUser2();
        User mockUserToBeUpdated = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updateBlocked(executingUser, mockUserToBeUpdated.getId()));
    }

    @Test
    public void updateBlocked_should_return_block_when_executingUserIsAdmin() {
        // Arrange
        User executingUser = createMockAdmin();
        User mockUserToBeUpdated = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        service.updateBlocked(executingUser, mockUserToBeUpdated.getId());

        Assertions.assertTrue(mockUserToBeUpdated.getIsBlocked());
    }

    @Test
    public void updatePhoneNumber_should_return_update_when_executingUserIsAdmin_and_UserToUpdateIsAdmin() {
        User executingUser = createMockAdmin();
        User mockUserToBeUpdated = createMockAdmin();
        PhoneNumber mockPhone = createMockPhoneNumber();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);
        Mockito.when(phoneNumberRepository.getByPhoneNumber(mockPhone.getPhoneNumber()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(phoneNumberRepository.getByUserId(mockPhone.getUserId()))
                .thenReturn(mockPhone);

        service.updatePhoneNumber(executingUser, mockUserToBeUpdated.getId(), mockPhone);

        Mockito.verify(phoneNumberRepository, Mockito.times(1))
                .update(mockPhone);
    }

    @Test
    public void updatePhoneNumber_should_return_throw_when_UserToUpdate_isNotAdmin() {
        User executingUser = createMockAdmin();
        User mockUserToBeUpdated = createMockUser();
        PhoneNumber mockPhone = createMockPhoneNumber();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updatePhoneNumber(executingUser, mockUserToBeUpdated.getId(), mockPhone));
        //is this test OK?
    }

    @Test
    public void updatePhoneNumber_should_return_throw_when_executingUser_isNotAdmin() {
        User executingUser = createMockUser2();
        User mockUserToBeUpdated = createMockAdmin();
        PhoneNumber mockPhone = createMockPhoneNumber();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updatePhoneNumber(executingUser, mockUserToBeUpdated.getId(), mockPhone));
    }

    @Test
    public void updatePhoneNumber_should_return_throw_when_PhoneAlreadyExists() {
        User executingUser = createMockAdmin();
        User mockUserToBeUpdated = createMockAdmin();
        PhoneNumber mockPhone = createMockPhoneNumber();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.updatePhoneNumber(executingUser, mockUserToBeUpdated.getId(), mockPhone));
    }

    @Test
    public void updateDeleted_should_return_update_when_executingUserIsAdmin() {
        // Arrange
        User executingUser = createMockAdmin();
        User mockUserToBeUpdated = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        service.delete(executingUser, mockUserToBeUpdated.getId());

        Assertions.assertTrue(mockUserToBeUpdated.isDeleted());
    }

    @Test
    public void updateDeleted_should_return_update_when_executingUserIsOwner() {
        // Arrange
        User executingUser = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(executingUser);

        service.delete(executingUser, executingUser.getId());

        Assertions.assertTrue(executingUser.isDeleted());
    }


    @Test
    public void updateDeleted_should_throw_when_executingUserIsNotAdmin() {
        // Arrange
        User executingUser = createMockUser2();
        User mockUserToBeUpdated = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(executingUser, mockUserToBeUpdated.getId()));
    }


}
