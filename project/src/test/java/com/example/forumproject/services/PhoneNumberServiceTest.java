package com.example.forumproject.services;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.PhoneNumber;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.PhoneNumberRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.forumproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PhoneNumberServiceTest {
    @Mock
    PhoneNumberRepository mockRepository;

    @InjectMocks
    PhoneNumberServiceImpl service;

    @Test
    public void getByPhoneNumber_Should_ReturnPhone_When_PhoneExists() {
        PhoneNumber mockPhoneNumber = createMockPhoneNumber();
        Mockito.when(mockRepository.getByPhoneNumber(mockPhoneNumber.getPhoneNumber()))
                .thenReturn(mockPhoneNumber);

        PhoneNumber result = service.getByPhoneNumber(mockPhoneNumber.getPhoneNumber());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockPhoneNumber.getPhoneNumber(), result.getPhoneNumber())
        );
    }
}
