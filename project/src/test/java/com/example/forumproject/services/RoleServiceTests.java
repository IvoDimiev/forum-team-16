package com.example.forumproject.services;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.Category;
import com.example.forumproject.models.Role;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.forumproject.Helpers.*;
import static com.example.forumproject.Helpers.createMockCategory;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTests {

    @Mock
    RoleRepository mockRepository;

    @InjectMocks
    RoleServiceImpl service;


    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_category_when_RoleExists() {
        // Arrange
        Role mockRole = createMockRole("user");

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockRole);
        // Act
        Role result = service.getById(mockRole.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRole.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRole.getRoleName(), result.getRoleName())
        );
    }

    @Test
    public void createRole_should_throw_when_userToExecuteIsBlocked() {
        // Arrange
        User mockUser = createMockAdmin();
        Role mockRole = createMockRole("user");
        mockUser.setBlocked(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockRole, mockUser));
    }

    @Test
    public void createRole_should_throw_when_userToExecuteIsNotAdmin() {
        // Arrange
        User mockUser = createMockUser();
        Role mockRole = createMockRole("user");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockRole, mockUser));
    }

    @Test
    public void createRole_should_throw_when_userToExecuteIsDeleted() {
        // Arrange
        User mockUser = createMockAdmin();
        Role mockRole = createMockRole("user");
        mockUser.setDeleted(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockRole, mockUser));
    }

    @Test
    public void createRole_should_throw_when_RoleAlreadyExists() {
        // Arrange
        User mockUser = createMockAdmin();
        Role mockRole = createMockRole("user");

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockRole, mockUser));
    }

    @Test
    public void createRole_should_callRepository_when_UserIsAdmin() {
        // Arrange
        User mockUser = createMockAdmin();
        Role mockRole = createMockRole("user");

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockRole, mockUser);
        // Act, Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockRole);
    }

    @Test
    public void updateRole_should_throw_when_userToExecuteIsBlocked() {
        // Arrange
        User mockUser = createMockAdmin();
        Role mockRole = createMockRole("user");
        mockRole.setRoleName("moderator");
        mockUser.setBlocked(true);


        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockRole, mockUser));
    }

    @Test
    public void updateRole_should_throw_when_userToExecuteIsNotAdmin() {
        // Arrange
        User mockUser = createMockUser();
        Role mockRole = createMockRole("user");
        mockRole.setRoleName("moderator");


        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockRole, mockUser));
    }

    @Test
    public void updateRole_should_throw_when_userToExecuteIsDeleted() {
        // Arrange
        User mockUser = createMockAdmin();
        Role mockRole = createMockRole("user");
        mockRole.setRoleName("moderator");
        mockUser.setDeleted(true);


        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockRole, mockUser));
    }

    @Test
    public void updateRole_should_throw_when_RoleAlreadyExists() {
        // Arrange
        User mockUser = createMockAdmin();
        Role mockRole = createMockRole("user");




        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockRole, mockUser));
    }

    @Test
    public void updateRole_should_callRepository_when_UserIsAdmin() {
        // Arrange
        User mockUser = createMockAdmin();
        Role mockRole = createMockRole("user");
        mockRole.setRoleName("moderator");

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        service.update(mockRole, mockUser);

        // Act, Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockRole);
    }
}
