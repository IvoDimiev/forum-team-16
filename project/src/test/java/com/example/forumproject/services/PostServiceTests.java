package com.example.forumproject.services;

import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.*;
import com.example.forumproject.repositories.contracts.CategoryRepository;
import com.example.forumproject.repositories.contracts.CommentRepository;
import com.example.forumproject.repositories.contracts.PostRepository;
import com.example.forumproject.repositories.contracts.TagRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.Category;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.Tag;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.CategoryRepository;
import com.example.forumproject.repositories.contracts.CommentRepository;
import com.example.forumproject.repositories.contracts.PostRepository;
import com.example.forumproject.repositories.contracts.TagRepository;
import com.example.forumproject.services.PostServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.constraints.AssertTrue;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.example.forumproject.Helpers.*;
import static com.example.forumproject.Helpers.createMockAdmin;

@ExtendWith(MockitoExtension.class)
public class PostServiceTests {
    @Mock
    PostRepository mockRepository;

    @Mock
    CategoryRepository categoryRepository;

    @Mock
    CommentRepository commentRepository;

    @Mock
    TagRepository mockTagRepository;

    @InjectMocks
    PostServiceImpl service;

    @Test
    void search_should_callRepository() {
        Mockito.when(mockRepository.search(null))
                .thenReturn(new ArrayList<>());

        service.search(null);

        Mockito.verify(mockRepository, Mockito.times(1))
                .search(null);
    }

    @Test
    void getTenMostRecent_should_callRepository() {
        Mockito.when(mockRepository.getTenMostRecent())
                .thenReturn(new ArrayList<>());

        service.getTenMostRecent();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getTenMostRecent();
    }

    @Test
    void getTenMostCommented_should_callRepository() {
        Mockito.when(mockRepository.getMostCommentedPosts())
                .thenReturn(new ArrayList<>());


        service.getMostCommentedPosts();


        Mockito.verify(mockRepository, Mockito.times(1))
                .getMostCommentedPosts();
    }

    @Test
    public void getById_Should_ReturnPost_When_PostExists() {
        Post mockPost = createMockPost();
        Mockito.when(mockRepository.getById(mockPost.getId()))
                .thenReturn(mockPost);

        Post result = service.getById(mockPost.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockPost.getId(), result.getId()),
                () -> Assertions.assertEquals(mockPost.getCategory().getName(), result.getCategory().getName()),
                () -> Assertions.assertEquals(mockPost.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockPost.getContent(), result.getContent()),
                () -> Assertions.assertEquals(mockPost.getCreationDate(), result.getCreationDate()),
                () -> Assertions.assertEquals(mockPost.getUser().getUsername(), result.getUser().getUsername()),
                () -> Assertions.assertEquals(mockPost.getComments().size(), result.getComments().size()),
                () -> Assertions.assertEquals(mockPost.getTags().size(), result.getTags().size()),
                () -> Assertions.assertEquals(mockPost.getVotes().size(), result.getVotes().size())
        );
    }

    @Test
    public void createPost_Should_Throw_When_UserIsDeleted() {
        Post mockPost = createMockPost();
        mockPost.getUser().setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockPost));
    }

    @Test
    public void createPost_Should_Throw_When_UserIsBlocked() {
        Post mockPost = createMockPost();
        mockPost.getUser().setBlocked(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockPost));
    }

    @Test
    public void createPost_Should_CallRepository_When_TheCreatorIsNotBlockedOrDeleted() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();

        service.create(mockPost);


        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockPost);
    }

    @Test
    public void votePost_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockUser.setDeleted(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.votePost(mockUser, mockPost.getId()));
    }

    @Test
    public void votePost_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockUser.setBlocked(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.votePost(mockUser, mockPost.getId()));
    }

    @Test
    public void votePost_should_vote_when_UserIsNotBlockedOrDeleted() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        service.votePost(mockUser, mockPost.getId());
        service.votePost(mockUser, mockPost.getId());

        Mockito.verify(mockRepository, Mockito.times(2))
                .update(mockPost);
    }

    @Test
    public void updatePost_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockUser.setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockPost, mockUser));
    }

    @Test
    public void updatePost_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockUser.setBlocked(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockPost, mockUser));
    }

    @Test
    public void updatePost_should_throw_when_executingUserIsNotAdmin() {
        User executingUser = createMockUser2();
        Post mockPost = createMockPost();


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockPost, executingUser));
    }

    @Test
    public void updatePost_should_return_update_when_executingUserIsAdmin() {
        User executingUser = createMockAdmin();
        Post mockPost = createMockPost();

        service.update(mockPost, executingUser);


        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockPost);
    }

    @Test
    public void updatePost_should_return_update_when_executingUserIsOwner() {
        Post mockPost = createMockPost();
        User executingUser = mockPost.getUser();
        mockPost.setId(55);

        service.update(mockPost, executingUser);


        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockPost);
    }

    @Test
    public void deletePost_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockUser.setDeleted(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockPost.getId(), mockUser));
    }

    @Test
    public void deletePost_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockUser.setBlocked(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockPost.getId(), mockUser));
    }

    @Test
    public void deletePost_should_throw_when_executingUserIsNotAdmin() {
        User mockUser = createMockUser2();
        Post mockPost = createMockPost();
        mockUser.setBlocked(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockPost.getId(), mockUser));
    }

    @Test
    public void deletePost_should_return_delete_when_executingUserIsAdmin() {
        User executingUser = createMockAdmin();
        Post mockPost = createMockPost();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        service.delete(mockPost.getId(), executingUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockPost);
    }

    @Test
    public void deletePost_should_return_delete_when_executingUserIsOwner() {
        Post mockPost = createMockPost();
        User executingUser = mockPost.getUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        service.delete(mockPost.getId(), executingUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockPost);
    }

    @Test
    public void createTagInPost_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockPost.setUser(mockUser);
        Tag mockedTag = createMockTag();
        mockUser.setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.createTagInPost(mockedTag, mockUser, mockPost.getId()));
    }

    @Test
    public void createTagInPost_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockPost.setUser(mockUser);
        Tag mockedTag = createMockTag();
        mockUser.setBlocked(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.createTagInPost(mockedTag, mockUser, mockPost.getId()));
    }

    @Test
    public void createTagInPost_should_throw_when_userToExecuteIsNotAuthorOrAdmin() {
        User mockUser = createMockUser2();
        Post mockPost = createMockPost();
        Tag mockedTag = createMockTag();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.createTagInPost(mockedTag, mockUser, mockPost.getId()));
    }

    @Test
    public void createTagInPost_should_return_create_whenExecutingUserIsNotBlockedOrDeleted() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockPost.setUser(mockUser);
        Tag mockedTag = createMockTag();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        service.createTagInPost(mockedTag, mockUser, mockPost.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockPost);
    }

    @Test
    public void createTagInPost_should_return_create_whenExecutingUserIsAdmin() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockPost.setUser(mockUser);
        Tag mockedTag = createMockTag();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        service.createTagInPost(mockedTag, mockUser, mockPost.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockPost);
    }

    @Test
    public void deleteTagInPost_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockPost.setUser(mockUser);
        Tag mockedTag = createMockTag();
        mockPost.addTag(mockedTag);
        mockUser.setDeleted(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.deleteTagInPost(mockedTag, mockUser, mockPost.getId()));
    }

    @Test
    public void deleteTagInPost_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockPost.setUser(mockUser);
        Tag mockedTag = createMockTag();
        mockUser.setBlocked(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.deleteTagInPost(mockedTag, mockUser, mockPost.getId()));
    }

    @Test
    public void deleteTagInPost_should_throw_when_userToExecuteIsNotAuthorOrAdmin() {
        User mockUser = createMockUser2();
        Post mockPost = createMockPost();
        Tag mockedTag = createMockTag();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.deleteTagInPost(mockedTag, mockUser, mockPost.getId()));

    }

    @Test
    public void deleteTagInPost_should_throw_when_tagIsNotInPost() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        Tag mockedTag = createMockTag();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);
        Mockito.when(mockTagRepository.getByName(Mockito.anyString()))
                .thenReturn(mockedTag);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.deleteTagInPost(mockedTag, mockUser, mockPost.getId()));

    }

    @Test
    public void deleteTagInPost_should_return_delete_whenExecutingUserIsNotBlockedOrDeleted() {
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        mockPost.setUser(mockUser);
        Tag mockedTag = createMockTag();
        mockPost.addTag(mockedTag);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Mockito.when(mockTagRepository.getByName(Mockito.anyString()))
                        .thenReturn(mockedTag); // WHY IS THIS RUINING EVERYTHING ??? QUSTION

        service.deleteTagInPost(mockedTag, mockUser, mockPost.getId());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockPost);    }

    @Test
    public void updateCategory_should_return_update_when_executingUserIsAdmin() {
        // Arrange
        User executingUser = createMockAdmin();
        Post mockPost = createMockPost();
        Category category = createMockCategory();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);
        Mockito.when(categoryRepository.getByName(Mockito.anyString()))
                .thenReturn(category);
        // Act
        service.updatePostCategory(category, mockPost.getId(), executingUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockPost);
    }

    @Test
    public void updateCategory_should_throw_when_executingUserIsNotAdminOrAuthor() {
        // Arrange
        User executingUser = createMockUser2();
        Post mockPost = createMockPost();
        Category category = createMockCategory();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);
        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updatePostCategory(category, mockPost.getId(), executingUser));
    }

}
