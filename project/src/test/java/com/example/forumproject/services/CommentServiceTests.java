package com.example.forumproject.services;

import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.*;
import com.example.forumproject.repositories.contracts.CommentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.forumproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CommentServiceTests {

    @Mock
    CommentRepository mockRepository;

    @InjectMocks
    CommentServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_Comment_when_commentExists() {
       Comment mockComment = createMockComment();
        Mockito.when(mockRepository.getById(mockComment.getId()))
                .thenReturn(mockComment);
        Comment result = service.getById(mockComment.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockComment.getId(), result.getId()),
                () -> Assertions.assertEquals(mockComment.getContent(), result.getContent()),
                () -> Assertions.assertEquals(mockComment.getUser(),result.getUser())
        );
    }

    @Test
    public void createComment_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();
        mockUser.setBlocked(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.createComment(mockComment,mockUser));
    }

    @Test
    public void createComment_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();
        mockUser.setDeleted(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.createComment(mockComment,mockUser));
    }

    @Test
    public void create_should_callRepository_when_theCreatorIsNotBlockedOrDeleted() {
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();

        service.createComment(mockComment,mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .createComment(mockComment);
    }

    @Test
    public void voteComment_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();
        mockUser.setDeleted(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.voteComment(mockUser, mockComment.getId()));
    }

    @Test
    public void voteComment_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();
        mockUser.setBlocked(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.voteComment(mockUser, mockComment.getId()));
    }

    @Test
    public void voteComment_should_vote_when_UserIsNotBlockedOrDeleted() {
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        service.voteComment(mockUser, mockComment.getId());
        service.voteComment(mockUser, mockComment.getId());

        Mockito.verify(mockRepository, Mockito.times(2))
                .updateComment(mockComment);
    }

    @Test
    public void updateComment_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();
        mockUser.setDeleted(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updateComment(mockComment,mockUser));
    }

    @Test
    public void updateComment_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();
        mockUser.setBlocked(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updateComment(mockComment,mockUser));
    }

    @Test
    public void updateComment_should_throw_when_executingUserIsNotAdmin() {
        User executingUser = createMockUser2();
        Comment mockComment = createMockComment();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updateComment(mockComment, executingUser));
    }

    @Test
    public void updateComment_should_return_update_when_executingUserIsAdmin() {
        User executingUser = createMockAdmin();
        Comment mockComment = createMockComment();
        mockComment.setContent("newMockComent content Mock Test Content");

        service.updateComment(mockComment, executingUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .updateComment(mockComment);

    }

    @Test
    public void updateComment_should_return_update_when_executingUserIsOwner() {
        Comment mockComment = createMockComment();
        User executingUser = mockComment.getUser();
        mockComment.setId(55);

        service.updateComment( mockComment, executingUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .updateComment(mockComment);
    }

    @Test
    public void deleteComment_should_throw_when_userToExecuteIsDeleted() {
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();
        mockUser.setDeleted(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.deleteComment(mockComment.getId(), mockUser));
    }

    @Test
    public void deleteComment_should_throw_when_userToExecuteIsBlocked() {
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();
        mockUser.setBlocked(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.deleteComment(mockComment.getId(), mockUser));
    }

    @Test
    public void deleteComment_should_throw_when_executingUserIsNotAdmin() {
        User executingUser = createMockUser2();
        Comment mockComment = createMockComment();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.deleteComment(mockComment.getId(), executingUser));
    }
    @Test
    public void deleteComment_should_return_delete_when_executingUserIsAdmin() {
        User executingUser = createMockAdmin();
        Comment mockComment = createMockComment();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        service.deleteComment(mockComment.getId(), executingUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .deleteComment(mockComment);
    }

    @Test
    public void deleteComment_should_return_delete_when_executingUserIsOwner() {
        Comment mockComment = createMockComment();
        User executingUser = mockComment.getUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        service.deleteComment(mockComment.getId(), executingUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .deleteComment(mockComment);
    }

}
