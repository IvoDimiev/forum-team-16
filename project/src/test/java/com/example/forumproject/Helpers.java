package com.example.forumproject;

import com.example.forumproject.models.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class Helpers {
    public static User createMockUser() {
        return createMockUser("user");
    }

    public static User createMockAdmin() {
        return createMockUser("admin");
    }

    private static User createMockUser(String role) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setCreationDate(LocalDateTime.now());
        mockUser.setBlocked(false);
        mockUser.setDeleted(false);
        mockUser.setRole(createMockRole(role));
        return mockUser;
    }

    public static User createMockUser2() {
        var mockUser = new User();
        mockUser.setId(2);
        mockUser.setEmail("moc2@user.com");
        mockUser.setUsername("MockUsername2");
        mockUser.setLastName("MockLastName2");
        mockUser.setPassword("MockPassword2");
        mockUser.setFirstName("MockFirstName2");
        mockUser.setLastName("MockLastName2");
        mockUser.setCreationDate(LocalDateTime.now());
        mockUser.setBlocked(false);
        mockUser.setDeleted(false);
        mockUser.setRole(createMockRole("user"));
        return mockUser;
    }

    public static Role createMockRole(String role) {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setRoleName(role);
        return mockRole;
    }

    public static PhoneNumber createMockPhoneNumber() {
        var mockPhone = new PhoneNumber();
        mockPhone.setUserId(1);
        mockPhone.setPhoneNumber("2312323113");
        return mockPhone;
    }
    public static Comment createMockComment() {
        var mockComment = new Comment();
        mockComment.setId(1);
        mockComment.setUser(createMockUser());
        mockComment.setContent("Content Content Content Content Content Content Content Content");
        mockComment.setCreationDate(LocalDateTime.now());
        mockComment.setPost(createMockPost());
        mockComment.setVotes(new HashSet<>());
        return mockComment;
    }

    public static Post createMockPost(){
        var mockPost = new Post();
        Comment comment = new Comment();
        mockPost.setId(1);
        mockPost.setCreationDate(LocalDateTime.now());
        mockPost.setVotes(new HashSet<>());
        mockPost.setTags(new HashSet<>());
        mockPost.setComments(new HashSet<>());
        mockPost.getComments().add(comment);
        mockPost.setUser(createMockUser());
        mockPost.setContent("Content Content Content Content Content Content Content Content");
        mockPost.setTitle("MockTitle MockTitle");
        mockPost.setCategory(createMockCategory());
        return mockPost;
    }

    public static Category createMockCategory(){
        var mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("MockCategory");
        return mockCategory;
    }

    public static Tag createMockTag(){
        var mockTag = new Tag();
        mockTag.setId(1);
        mockTag.setTagName("TagName");
        return mockTag;
    }
}
