// Create post form
const addTagBtn = document.querySelector("#addTag_btn")
let allTags = []
addTagBtn.addEventListener("click", function (e) {
    e.preventDefault()
    const tagInput = document.querySelector("#tag_input")
    const tagValue = tagInput.value.toLowerCase().trim().replace(" ", "-");
    tagInput.value = ""

    if(!allTags.includes(tagValue) && tagValue.length > 0) {
        allTags.push(tagValue)

        const allTagsHiddenInput = document.querySelector("#allTags_input")
        allTagsHiddenInput.value += tagValue + ";"

        const tagsContainer = document.querySelector("#tags_container")
        const tag = document.createElement("div")
        tag.setAttribute("id", tagValue)
        const closeButtonId = "removeTag_btn_" + tagValue
        tag.innerHTML = `
        <span>${tagValue}</span>
        <span class="removeTag_btn" data-tag="${tagValue}" id="${closeButtonId}">X</span>
    `
        tag.classList.add("tagPreview_container")
        tagsContainer.append(tag)

        const removeTagBtn = document.querySelector("#removeTag_btn_" + tagValue)
        removeTagBtn.addEventListener("click", function (e) {
            const clickedBtn = e.target
            const clickedBtnValue = clickedBtn.dataset.tag
            allTagsHiddenInput.value = allTagsHiddenInput.value.replace(clickedBtnValue + ";", "")
            const tagToRemove = document.getElementById(clickedBtnValue)
            allTags = allTags.filter(tag => tag !== clickedBtnValue)
            tagToRemove.remove()
        })
    }
})

const formCloseBtn = document.querySelector("#createPost_close_btn")

formCloseBtn.addEventListener("click", function () {
    const popupWrapper = document.querySelector(".popup_wrapper")
    popupWrapper.classList.add("hide")
})


const showFormBtn = document.querySelector("#show_form")
if(showFormBtn) {
    showFormBtn.addEventListener("click", function () {
        const popupWrapper = document.querySelector(".popup_wrapper")
        popupWrapper.classList.remove("hide")
    })
}

const creationFormPopupBackground = document.querySelector("#creationForm_background")
creationFormPopupBackground.addEventListener("click", function () {
    const popupWrapper = document.querySelector(".popup_wrapper")
    popupWrapper.classList.add("hide")
})

const createPostButton = document.querySelector("#createPost_btn")
createPostButton.addEventListener("click",function (e) {
    const title = document.querySelector("#createPost_form").title.value
    const content = document.querySelector("#createPost_form").content.value
    const titleErrorLabel = document.querySelector("#titleError_label")
    const contentErrorLabel = document.querySelector("#contentError_label")
    titleErrorLabel.classList.add("hide")
    contentErrorLabel.classList.add("hide")

    if( title==null || title.length > 64 || title.length < 16){
        e.preventDefault()
        titleErrorLabel.classList.remove("hide")
    }
    if( content==null || content.length > 8192 || content.length < 32){
        e.preventDefault()
        contentErrorLabel.classList.remove("hide")
    }
})

// filter
var reset_input_values = document.querySelectorAll('.filter_input');
for (var i = 0; i < reset_input_values.length; i++) {
    reset_input_values[i].value = '';
}
let currentSelectedOption = "#username"
function filter(event) {
    const nextValue = document.querySelector(currentSelectedOption).value
    document.querySelector(currentSelectedOption).value = ''

    const optionValue = event.target.value.toLowerCase()
    currentSelectedOption = "#" + optionValue
    console.log(currentSelectedOption)
    document.querySelector(currentSelectedOption).value = nextValue
}

function mapSearchValue(event){
    const hiddenField = document.querySelector(currentSelectedOption)
    hiddenField.value = event.target.value
}

