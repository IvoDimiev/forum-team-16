$('#comment_textarea').keyup(function() {

    var characterCount = $(this).val().length,
        current = $('#current'),
        maximum = $('#maximum'),
        theCount = $('#the-count');

    current.text(characterCount);

    /*This isn't entirely necessary, just playin around*/
    if (characterCount < 70) {
        current.css('color', '#666');
    }
    if (characterCount > 70 && characterCount < 90) {
        current.css('color', '#6d5555');
    }
    if (characterCount > 90 && characterCount < 100) {
        current.css('color', '#793535');
    }
    if (characterCount > 100 && characterCount < 120) {
        current.css('color', '#841c1c');
    }
    if (characterCount > 120 && characterCount < 139) {
        current.css('color', '#8f0001');
    }

    if (characterCount >= 140) {
        maximum.css('color', '#8f0001');
        current.css('color', '#8f0001');
        theCount.css('font-weight','bold');
    } else {
        maximum.css('color','#666');
        theCount.css('font-weight','normal');
    }
});

// copy from: https://codepen.io/patrickwestwood/pen/gPPywv

//
// //post like button
// const postLikeBtns = document.querySelectorAll(".postLike_icon")
//
// for (let i = 0; i < postLikeBtns.length; i++) {
//     const likeBtn = postLikeBtns.item(i)
//     likeBtn.addEventListener("click", function (e) {
//         const target = e.target
//         const postId = target.dataset.post_id
//         //TODO - use user id to authorize
//         const userId = target.dataset.user_id
//
//         $.ajax
//         ({
//             type: "POST",
//             url: `http://localhost:8080/api/posts/${postId}/vote`,
//             dataType: 'json',
//             headers: {
//                 "Authorization": "sofi"
//             },
//             data: "",
//             success: function (obj){
//                 updatePostVotesCount(obj)
//
//             },
//             error: function (XMLHttpRequest, textStatus, errorThrown) {
//                 alert("Sorry, you are blocked :(")
//                 console.log("error...", textStatus, errorThrown, XMLHttpRequest)
//             }
//         });
//     })
// }
// function updatePostVotesCount(post) {
//     const count = post.likesCount
//     const postLikeContainer = document.querySelector("#postId_" + post.id)
//     console.log(post)
//
//     const postVotesCountEl = postLikeContainer.parentElement.querySelector("#postVotes_count")
//     postVotesCountEl.innerHTML = count
//     postLikeContainer.querySelector(".postDislike_icon").classList.toggle('hide')
// }
//
// //comment like button
// const commentLikeBtns = document.querySelectorAll(".commentLike_icon")
//
// for (let i = 0; i < commentLikeBtns.length; i++) {
//     const likeBtn = commentLikeBtns.item(i)
//     likeBtn.addEventListener("click", function (e) {
//         const target = e.target
//         const postId = target.dataset.post_id
//         //TODO - use user id to authorize
//         const userId = target.dataset.user_id
//         const commentId = target.dataset.comment_id
//
//         $.ajax
//         ({
//             type: "POST",
//             url: `http://localhost:8080/api/posts/${postId}/comments/${commentId}/vote`,
//             dataType: 'json',
//             headers: {
//                 "Authorization": "sofi"
//             },
//             data: "",
//             success: function (obj){
//                 updateCommentVotesCount(obj)
//
//             },
//             error: function (XMLHttpRequest, textStatus, errorThrown) {
//                 alert("Sorry, you are blocked :(")
//                 console.log("error...", textStatus, errorThrown, XMLHttpRequest)
//             }
//         });
//     })
// }
function updateCommentVotesCount(comment) {
    const commentContainer = document.querySelector("#commentId_" + comment.id)
    const count = comment.likesCount
    const commentVotesCountEl = commentContainer.parentElement.querySelector(".count")
    commentVotesCountEl.innerHTML = count
    commentContainer.querySelector(".commentDislike_icon").classList.toggle('hide')
}
