package com.example.forumproject.models.dtos;

public class CategoryOutDto {
    private int id;
    private String name;
    private int numberOfPosts;

    public CategoryOutDto() {
    }

    public CategoryOutDto(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfPosts() {
        return numberOfPosts;
    }

    public void setNumberOfPosts(int numberOfPosts) {
        this.numberOfPosts = numberOfPosts;
    }
}
