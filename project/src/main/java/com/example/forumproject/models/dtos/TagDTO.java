package com.example.forumproject.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TagDTO {
    @NotNull
    @Size(min = 3, max = 15, message = "Tag should be between 3 and 15 symbols")
    private String tagName;

    public TagDTO() {
    }

    public TagDTO(int id, String tagName) {
        this.tagName = tagName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

}
