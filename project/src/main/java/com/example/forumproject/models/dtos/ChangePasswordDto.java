package com.example.forumproject.models.dtos;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ChangePasswordDto {
    @NotNull(message = "Password can't be empty")
    @Size(min = 8, max = 20, message = "Password be between 8 and 20 symbols")
    private String oldPassword;

    @NotNull(message = "Password can't be empty")
    @Size(min = 8, max = 20, message = "Password be between 8 and 20 symbols")
    private String newPassword;

    @NotNull(message = "Password can't be empty")
    @Size(min = 8, max = 20, message = "Password be between 8 and 20 symbols")
    private String confirmPassword;

    public ChangePasswordDto() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
