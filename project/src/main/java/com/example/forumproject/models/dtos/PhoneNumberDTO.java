package com.example.forumproject.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PhoneNumberDTO {
    @NotNull(message = "Phone number should not be null")
    @Size(min = 8, max = 10, message = "Phone number should be between 8 and 10 symbols")
    private String phoneNumber;

    public PhoneNumberDTO() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
