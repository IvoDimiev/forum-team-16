package com.example.forumproject.models;

import javax.persistence.*;

@Entity
@Table(name = "images")
public class Image {
    @Id
    @Column(name = "user_id")
    private int userId;

    @Column(name = "image")
    private byte[] image;

    public Image() {
    }

    public Image(int userId, byte[] image) {
        this.userId = userId;
        this.image = image;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
