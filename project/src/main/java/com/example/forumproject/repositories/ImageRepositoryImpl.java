package com.example.forumproject.repositories;

import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.models.Image;
import com.example.forumproject.models.PhoneNumber;
import com.example.forumproject.models.Tag;
import com.example.forumproject.repositories.contracts.ImageRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ImageRepositoryImpl implements ImageRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ImageRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Image getImage(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Image image = session.get(Image.class, userId);
            if (image == null) {
                throw new EntityNotFoundException("Image", userId);
            }
            return image;
        }
    }

    @Override
    public void create(Image image) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(image);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Image image) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(image);
            session.getTransaction().commit();
        }
    }
}
