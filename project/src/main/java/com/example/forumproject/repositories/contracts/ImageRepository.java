package com.example.forumproject.repositories.contracts;

import com.example.forumproject.models.Image;

public interface ImageRepository {
    Image getImage(int userId);

    void create(Image image);

    void update(Image image);
}
