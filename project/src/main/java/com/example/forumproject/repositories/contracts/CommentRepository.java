package com.example.forumproject.repositories.contracts;

import com.example.forumproject.models.Comment;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.User;

import java.util.List;

public interface CommentRepository {
    List<Comment> getAll();

    Comment getById(int id);

    void createComment(Comment comment);

    void updateComment(Comment comment);

    void deleteComment(Comment comment);

    List<Comment> getCommentsByPostId(int postId);
}
