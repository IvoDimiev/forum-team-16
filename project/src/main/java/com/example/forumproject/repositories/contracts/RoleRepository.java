package com.example.forumproject.repositories.contracts;

import com.example.forumproject.models.Role;


import java.util.List;

public interface RoleRepository {
    List<Role> getAll();

    Role getById(int id);

    Role getByName(String name);

    void create(Role role);

    void update(Role role);

}
