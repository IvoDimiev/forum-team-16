package com.example.forumproject.repositories.contracts;

import com.example.forumproject.models.Post;

import java.util.List;
import java.util.Optional;

public interface PostRepository {
    List<Post> getAll();

    Post getById(int id);

    void create(Post post);

    void update(Post post);

    void delete(Post post);

    List<Post> search(Optional<String> search);

    List<Post> filter(Optional<String> title, Optional<String> username, Optional<String> content, Optional<String> tagName,
                      Optional<String> sort);

    List<Post> getMostCommentedPosts();

    List<Post> getTenMostRecent();

    List<Post> getPostsByCategory(int id);

    List<Post> getRelatedPosts(List<String> tags, int id);
}
