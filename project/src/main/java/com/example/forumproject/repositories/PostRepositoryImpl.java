package com.example.forumproject.repositories;

import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.models.*;
import com.example.forumproject.repositories.contracts.PostRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;


@Repository
public class PostRepositoryImpl implements PostRepository {
    private final SessionFactory sessionFactory;

    public PostRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Post> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post", Post.class);
            return query.list();
        }
    }

    @Override
    public List<Post> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Post> query = session.createNativeQuery(
                    "Select pw.post_id, pw.title, pw.content, pw.creation_date, pw.category_id, pw.user_id from\n" +
                            "(SELECT p.post_id, p.title, p.content, p.creation_date, p.category_id , p.user_id, t.tag_name as tag \n" +
                            "from posts p\n" +
                            "join tag_post pt on p.post_id = pt.post_id\n" +
                            "join tags t on t.tag_id = pt.tag_id\n" +
                            ") as pw\n" +
                            "where pw.tag like :tagName"
            );
            query.addEntity(Post.class);
            query.setParameter("tagName", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<Post> getMostCommentedPosts() {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Post> query = session.createNativeQuery(
                    "select distinct p.post_id, p.title, p.content, p.creation_date, p.category_id, p.user_id, COUNT(*) as count " +
                            "from posts p " +
                            "join comments c on p.post_id = c.post_id " +
                            "group by c.post_id " +
                            "order by count desc " +
                            "limit 10;");
            query.addEntity(Post.class);
            return query.list();
        }
    }

    @Override
    public List<Post> getTenMostRecent() {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Post> query = session.createNativeQuery(
                    "select distinct p.post_id, p.title, p.content, p.creation_date, p.category_id, p.user_id " +
                            "from posts p " +
                            "order by creation_date desc " +
                            "limit 10;");
            query.addEntity(Post.class);
            return query.list();
        }
    }

    @Override
    public List<Post> getPostsByCategory(int id) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Post> query = session.createNativeQuery(
                    " select * from posts" +
                            " join categories c on c.category_id = posts.category_id " +
                            " where c.category_id = :id"
            );
            query.addEntity(Post.class);
            query.setParameter("id", id);
            return query.list();
      }
    }

    @Override
    public List<Post> getRelatedPosts(List<String> tags, int id) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Post> query = session.createNativeQuery(
                    "Select distinct selectedPosts.post_id, selectedPosts.user_id, selectedPosts.category_id,\n" +
                            "       selectedPosts.title, selectedPosts.content, selectedPosts.creation_date\n" +
                            "from (Select distinct p.post_id, user_id, category_id, title, content, creation_date\n" +
                            "      from posts p\n" +
                            "               join tag_post tp on p.post_id = tp.post_id\n" +
                            "               join tags t on t.tag_id = tp.tag_id\n" +
                            "      where t.tag_name in (:tags) and p.post_id != :id)  as selectedPosts\n" +
                            "left join posts_likes l on selectedPosts.post_id = l.post_id\n" +
                            "order by l.post_id desc limit 7;"
            );

//            NativeQuery<Post> query = session.createNativeQuery(
//                    "Select distinct p.post_id, user_id, category_id, title, content, creation_date\n" +
//                            "from posts p\n" +
//                            "         join tag_post tp on p.post_id = tp.post_id\n" +
//                            "         join tags t on t.tag_id = tp.tag_id\n" +
//                            "where t.tag_name in ('conflict') and p.post_id != 1"
//            ).setMaxResults(7);
            query.addEntity(Post.class);
            query.setParameter("tags", tags);
            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public List<Post> filter(Optional<String> title, Optional<String> username, Optional<String> content, Optional<String> tagName,
                             Optional<String> sort) {
        System.out.println("title" + title);
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder("select distinct p from Post p left join p.tags t ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            
            title.ifPresent(value -> {
                filter.add(" p.title like :title ");
                queryParams.put("title", "%" + value + "%");
            });

            username.ifPresent(value -> {
                filter.add(" p.user.username like :user ");
                queryParams.put("user", "%" + value + "%");
            });

            content.ifPresent(value -> {
                filter.add(" p.content like :content ");
                queryParams.put("content", "%" + value + "%");
            });

            tagName.ifPresent(value -> {
                filter.add(" t.tagName like :tagName ");
                queryParams.put("tagName", value);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            Query<Post> queryList = session.createQuery(queryString.toString(), Post.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");
        if (value.isEmpty()) {
            throw new UnsupportedOperationException("Sort should have at least 1 parameter");
        }
        switch (params[0]) {
            case "creationDate":
                queryString.append(" p.creationDate ");
                break;
            case "username":
                queryString.append(" p.user.username ");
                break;
            case "categoryName":
                queryString.append(" p.category.name ");
                break;
            case "likes":
                queryString.append(" p.votes.size");
                break;
        }
        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }
        return queryString.toString();
    }

    @Override
    public Post getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Post post = session.get(Post.class, id);
            if (post == null) {
                throw new EntityNotFoundException("Post", id);
            }
            return post;
        }
    }

    @Override
    public void create(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Post post) {
       try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(post);
            session.getTransaction().commit();
        }
    }
}
