package com.example.forumproject.repositories.contracts;

import com.example.forumproject.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getAll();

    User getById(int id);

    void create(User user);

    void update(User user);

    User getByEmail(String email);

    User getByUsername(String username);

    List<User> filter(Optional<String> username, Optional<String> email, Optional<String> firstName, Optional<String> lastName, Optional<String> sort);
}
