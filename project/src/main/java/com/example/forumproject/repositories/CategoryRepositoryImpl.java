package com.example.forumproject.repositories;

import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.models.Category;
import com.example.forumproject.models.Post;
import com.example.forumproject.repositories.contracts.CategoryRepository;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category", Category.class);
            return query.list();
        }
    }

    @Override
    public Category getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Category category = session.get(Category.class, id);
            if (category == null) {
                throw new EntityNotFoundException("Category", id);
            }
            return category;
        }
    }

    @Override
    public Category getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category where name = :name", Category.class);
            query.setParameter("name", name);
            List<Category> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Category", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public int getPostsCountInCategory(int categoryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post p where p.category.id = :categoryId", Post.class);
            query.setParameter("categoryId",categoryId);
            return query.list().size();
        }
    }

    @Override
    public void create(Category category) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(category);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Category category) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(category);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Category categoryToDelete) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(categoryToDelete);
            session.getTransaction().commit();
        }
    }

}
