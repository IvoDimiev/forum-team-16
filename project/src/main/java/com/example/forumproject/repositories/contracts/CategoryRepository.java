package com.example.forumproject.repositories.contracts;

import com.example.forumproject.models.Category;

import java.util.List;

public interface CategoryRepository {
    List<Category> getAll();

    Category getById(int id);

    void create(Category category);

    void update(Category category);

    void delete(Category categoryToDelete);

    Category getByName(String name);

    int getPostsCountInCategory(int categoryId);
}
