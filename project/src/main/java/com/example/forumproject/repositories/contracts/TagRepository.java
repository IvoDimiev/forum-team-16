package com.example.forumproject.repositories.contracts;

import com.example.forumproject.models.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getAll();

    Tag getById(int id);

    Tag getByName(String name);

    void create(Tag tag);

    void update(Tag tag);

    void delete(Tag tagToBeDeleted);
}
