package com.example.forumproject.repositories.contracts;

import com.example.forumproject.models.PhoneNumber;

public interface PhoneNumberRepository {

    PhoneNumber create(PhoneNumber phoneNumber);

    void update(PhoneNumber phoneNumber);

    void delete(PhoneNumber phoneNumber);

    PhoneNumber getByPhoneNumber(String number);

    PhoneNumber getByUserId (int id);
}
