package com.example.forumproject.controllers.rest;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.utils.CategoryMapper;
import com.example.forumproject.models.Category;
import com.example.forumproject.models.User;
import com.example.forumproject.models.dtos.CategoryDTO;
import com.example.forumproject.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {
    private final CategoryService service;
    private final AuthenticationHelper authenticationHelper;
    private final CategoryMapper categoryMapper;

    @Autowired
    public CategoryController(CategoryService service, AuthenticationHelper authenticationHelper, CategoryMapper categoryMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.categoryMapper = categoryMapper;
    }

    @GetMapping
    public List<Category> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Category getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Category create(@RequestHeader HttpHeaders headers, @Valid @RequestBody CategoryDTO categoryDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Category category = categoryMapper.fromDto(categoryDTO);
            service.create(category, user);
            return category;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Category update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                           @Valid @RequestBody CategoryDTO categoryDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Category category = categoryMapper.fromDto(categoryDTO, id);
            service.update(category, user);
            return category;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
