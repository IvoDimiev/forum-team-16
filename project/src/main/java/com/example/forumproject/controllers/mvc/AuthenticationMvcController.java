package com.example.forumproject.controllers.mvc;

import com.example.forumproject.controllers.rest.AuthenticationHelper;
import com.example.forumproject.exceptions.AuthenticationFailureException;
import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.models.User;
import com.example.forumproject.models.dtos.LoginUserDto;
import com.example.forumproject.models.dtos.RegisterUserDTO;
import com.example.forumproject.services.contracts.UserService;
import com.example.forumproject.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {
    private final UserMapper userMapper;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AuthenticationMvcController(UserMapper userMapper, UserService userService, AuthenticationHelper authenticationHelper) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            //return "redirect:/auth/login";
        }
        model.addAttribute("login", new LoginUserDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLoginPage(@ModelAttribute("login") LoginUserDto loginUserDto, BindingResult bindingResult,
                                  HttpSession session, HttpServletResponse response){
       if(bindingResult.hasErrors()){
           return "login";
       }

       try {
           authenticationHelper.verifyAuthentication(loginUserDto.getUsername(), loginUserDto.getPassword());
           session.setAttribute("currentUser", loginUserDto.getUsername());
           Cookie cookie = new Cookie("user", loginUserDto.getUsername());
           cookie.setSecure(false);
           response.addCookie(cookie);
           return "redirect:/";
       } catch (AuthenticationFailureException e) {
           bindingResult.rejectValue("username", "auth_error", e.getMessage());
           return "login";
       }
    }
    @GetMapping("/logout")
    public String handleLogout(HttpSession session, HttpServletResponse response) {
        session.removeAttribute("currentUser");
        response.reset();
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterUserDTO());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterUserDTO registerUserDto,
                                 BindingResult bindingResult,
                                 HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        try {
            User user = userMapper.registerDTOToObject(registerUserDto);
            userService.create(user);
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            System.out.println("error here");
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        }
    }
}
