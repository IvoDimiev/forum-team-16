package com.example.forumproject.controllers.mvc;

import com.example.forumproject.controllers.rest.AuthenticationHelper;
import com.example.forumproject.exceptions.AuthenticationFailureException;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.User;
import com.example.forumproject.services.contracts.CategoryService;
import com.example.forumproject.services.contracts.PostService;
import com.example.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {
    private final CategoryService categoryService;
    private final UserService userService;
    private final PostService postService;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public HomeMvcController(CategoryService categoryService, UserService userService, PostService postService, AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.postService = postService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session) {
        List<Post> postResults =  postService.getAll();
        List<Post> mostCommentedPosts = postService.getMostCommentedPosts();
        List<Post> mostRecentPosts = postService.getTenMostRecent();


        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("usersSize", userService.getAll().size());
        model.addAttribute("postsSize", postService.getAll().size());
        model.addAttribute("posts", postResults);
        model.addAttribute("mostRecentPosts", mostRecentPosts);
        model.addAttribute("mostCommentedPosts", mostCommentedPosts);
        return "index";
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("getUserId")
    public int populateGetUser(HttpSession session) {
       return authenticationHelper.tryGetUserId(session);
    }
}
