package com.example.forumproject.controllers.mvc;

import com.example.forumproject.controllers.rest.AuthenticationHelper;
import com.example.forumproject.exceptions.AuthenticationFailureException;
import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.*;
import com.example.forumproject.models.dtos.*;
import com.example.forumproject.services.contracts.*;
import com.example.forumproject.utils.PhoneNumberMapper;
import com.example.forumproject.utils.PostMapper;
import com.example.forumproject.utils.UserMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final UserService userService;
    private final UserMapper mapper;
    private final PostService postService;
    private final CommentService commentService;
    private final PostMapper postMapper;
    private final RoleService roleService;
    private final PhoneNumberMapper phoneNumberMapper;
    private final PhoneNumberService phoneNumberService;
    private final AuthenticationHelper authenticationHelper;
    private final ImageService imageService;

    public UserMvcController(UserService userService, UserMapper mapper, PostService postService,
                             CommentService commentService, PostMapper postMapper, RoleService roleService,
                             PhoneNumberMapper phoneNumberMapper, PhoneNumberService phoneNumberService, AuthenticationHelper authenticationHelper, ImageService imageService) {
        this.userService = userService;
        this.mapper = mapper;
        this.postService = postService;
        this.commentService = commentService;
        this.postMapper = postMapper;
        this.roleService = roleService;
        this.phoneNumberMapper = phoneNumberMapper;
        this.phoneNumberService = phoneNumberService;
        this.authenticationHelper = authenticationHelper;
        this.imageService = imageService;
    }

    @GetMapping("/{id}")
    public String showUserProfile(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User existingUser = userService.getById(id);
            UserOutDTO userOutDTO = mapper.objectToDTO(existingUser);
            List<Post> userPosts = postService.filter(Optional.empty(), Optional.of(userOutDTO.getUsername()), Optional.empty(), Optional.empty(),
                    Optional.empty());
            //moje da se izmesti v pomagatelen sloi??
            List <PostOutDTO> posts = new ArrayList<>();
            String phoneNumber;
            try {
                phoneNumber = phoneNumberService.getByUserId(id).getPhoneNumber();
            } catch (EntityNotFoundException e) {
                if(existingUser.getRole().getRoleName().equals("admin")) {
                    phoneNumber = "none";
                } else {
                    phoneNumber = "only-user";
                }
            }
            userPosts.forEach(post -> posts.add(postMapper.objectToDTO(post)));
            model.addAttribute("user", userOutDTO);
            model.addAttribute("userId", id);
            model.addAttribute("postsCount", userPosts.size());
            model.addAttribute("commentsCount", getUserCommentsCount(id));
            model.addAttribute("posts", posts);
            model.addAttribute("phoneNumber", phoneNumber );
            model.addAttribute("isLoggedUserProfileOwner", id == user.getId());
            model.addAttribute("isUserAdmin", existingUser.getRole().getRoleName().equals("admin"));
            return "profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User existingUser = userService.getById(id);
            UserOutDTO userDTO = mapper.objectToDTO(existingUser);
            model.addAttribute("user", userDTO);
            model.addAttribute("userId", existingUser.getId());
            return "edit-user-profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update-password")
    public String showUpdatePasswordPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User existingUser = userService.getById(id);
            UserOutDTO userDTO = mapper.objectToDTO(existingUser);
            model.addAttribute("password", new ChangePasswordDto());
            model.addAttribute("user", user);
            return "edit-password";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update-password")
    public String changePassword (@Valid @ModelAttribute ("password") ChangePasswordDto changePasswordDto, BindingResult errors,
                                     @PathVariable int id, Model model, HttpSession session ) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("user", user);

        if(errors.hasErrors()) {
            return "edit-password";
        }

        String oldPassword = changePasswordDto.getOldPassword();
        String newPassword = changePasswordDto.getNewPassword();
        String confirmPassword = changePasswordDto.getConfirmPassword();

        try {
            userService.changePassword(user, oldPassword, newPassword, confirmPassword);
            return "redirect:/users/" + user.getId();
        } catch (UnauthorizedOperationException | InputMismatchException e) {
            errors.rejectValue("newPassword", "password_mismatch", e.getMessage());
            return "edit-password";
        }

    }

    @GetMapping("{id}/phone-number")
    public String createPhoneNumber (@PathVariable int id, Model model, HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User userToBeUpdated = userService.getById(id);
            model.addAttribute("phoneNumber", new PhoneNumberDTO());
            model.addAttribute("user",userToBeUpdated);
            return "phone-number";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("{id}/phone-number")
    public String createPhoneNumber (@Valid@ModelAttribute ("phoneNumber") PhoneNumberDTO phoneNumberDTO, BindingResult errors,
                                     @PathVariable int id, Model model, HttpSession session ) {
        User executingUser;
        User userToBeUpdated = userService.getById(id);
        try {
            executingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if(errors.hasErrors()) {
            model.addAttribute("user", userToBeUpdated);
            return "phone-number";
        }
        try {
            PhoneNumber phoneNumber = phoneNumberMapper.dtoToObject(phoneNumberDTO);
            userService.createPhoneNumber(executingUser,id,phoneNumber);
            model.addAttribute("user",userToBeUpdated);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            model.addAttribute("user", userToBeUpdated);
            errors.rejectValue("phoneNumber", "duplicate_phoneNumber", e.getMessage());
            return "phone-number";
        }
    }

    @GetMapping("{id}/phone-number/update")
    public String updatePhoneNumber (@PathVariable int id, Model model, HttpSession session) {
        User executingUser;
        try {
            executingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User userToBeUpdated = userService.getById(id);
            PhoneNumberDTO phoneNumberDTO = new PhoneNumberDTO();
            PhoneNumber phoneNumber = phoneNumberService.getByUserId(id);
            phoneNumberDTO.setPhoneNumber(phoneNumber.getPhoneNumber());
            model.addAttribute("phoneNumber", phoneNumberDTO);
            model.addAttribute("user", userToBeUpdated);
            return "phone-number";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("{id}/phone-number/update")
    public String updatePhoneNumber (@PathVariable int id, @Valid @ModelAttribute ("phoneNumber") PhoneNumberDTO phoneNumberDTO, BindingResult errors,
                                      Model model, HttpSession session) {
        User executingUser;
        User userToBeUpdated = userService.getById(id);
        try {
            executingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if(errors.hasErrors()) {
            model.addAttribute("user", userToBeUpdated);
            return "phone-number";
        }
        try {
            PhoneNumber phoneNumber = phoneNumberMapper.dtoToObject(phoneNumberDTO);
            userService.updatePhoneNumber(executingUser, id, phoneNumber);
            model.addAttribute("user",userToBeUpdated);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            model.addAttribute("user", userToBeUpdated);
            errors.rejectValue("phoneNumber", "duplicate_phoneNumber", e.getMessage());
            return "phone-number";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id, @Valid @ModelAttribute("updateUser") UserDTO userDTO, BindingResult errors,
                             Model model, HttpSession session) {
        User executingUser;
        try {
            executingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if(errors.hasErrors()) {
            User existingUser = userService.getById(id);
            model.addAttribute("user", existingUser);
            model.addAttribute("userId", existingUser.getId());
            return "edit-user-profile";
        }

        try {
            User userToBeUpdated = mapper.dtoToObject(userDTO, id);
            userService.update(executingUser, userToBeUpdated);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }


    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        User executingUser;
        try {
            executingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getById(id);
            userService.delete(user,id);
            return "redirect:/auth/logout";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{userToBeUpdatedId}/roles")
    public String changeUserRole(@Valid RoleDTO roleDTO, BindingResult errors, @PathVariable int userToBeUpdatedId,  HttpSession session,
                                 Model model) {
        User executingUser;
        try {
            executingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "profile";
        }

        try {
            Role role = roleService.getByName(roleDTO.getRoleName());
            userService.updateRole(executingUser, userToBeUpdatedId, role);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

        return "redirect:/users/" + userToBeUpdatedId;
    }

    @GetMapping("/{id}/block")
    public String blockUser(@PathVariable int id, Model model, HttpSession session) {

        User executingUser;
        try {
            executingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.updateBlocked(executingUser,id);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {

        User executingUser;
        try {
            executingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            model.addAttribute("users", userService.getAll());
            model.addAttribute("filterUserDto", new FilterUserDto());

            return "users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/image-upload")
    public String uploadImage(@PathVariable int id, @RequestParam("file") MultipartFile file, Model model)
            throws IOException {
        //file.getBytes()
        try {
            byte[] img = file.getBytes();
            imageService.saveImage(id, img);
            User existingUser = userService.getById(id);
            UserOutDTO userDTO = mapper.objectToDTO(existingUser);
            model.addAttribute("user", userDTO);
            model.addAttribute("userId", existingUser.getId());
            return "edit-user-profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/filter")
    public String filterPosts(@ModelAttribute("filterUserDto") FilterUserDto filterUserDto, Model model) {
        var filteredUsers = userService.filter(
                Optional.ofNullable(filterUserDto.getUsername().isBlank() ? null : filterUserDto.getUsername()),
                Optional.ofNullable(filterUserDto.getEmail().isBlank() ? null : filterUserDto.getEmail()),
                Optional.ofNullable(filterUserDto.getFirstName().isBlank() ? null : filterUserDto.getFirstName()),
                Optional.ofNullable(filterUserDto.getLastName().isBlank() ? null : filterUserDto.getLastName()),
                Optional.ofNullable(filterUserDto.getSort().isBlank() ? null : filterUserDto.getSort())
        );


        model.addAttribute("users", filteredUsers);
        return "users";
    }

    @GetMapping("/{id}/posts")
    public String showMyPosts(@PathVariable int id, Model model, HttpSession session) {
        User executingUser;
        try {
            executingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            User existingUser = userService.getById(id);
            List<Post> posts = postService.getAll();
            //TODO: refactor, use query
            List<Post> myPosts = postService.filter(
                    Optional.empty(), Optional.of(existingUser.getUsername()), Optional.empty(), Optional.empty(),
                    Optional.empty());
            //List<Post> myPosts = posts.stream().filter(post -> post.getUser().getId() == (existingUser.getId())).collect(Collectors.toList());
            model.addAttribute("posts", myPosts);
            model.addAttribute("newPost",  new PostCreationMvcDto());
            model.addAttribute("filterPostDto", new FilterPostDto());
            return "posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/image")
    public ResponseEntity<byte[]> getImage(@PathVariable int id) throws IOException {
            try {
                byte[] image = imageService.getImage(id).getImage();
                return new ResponseEntity<byte[]>(image, new HttpHeaders(), HttpStatus.OK);
            } catch (EntityNotFoundException e) {
                byte[] image = imageService.getImage(0).getImage();
                return new ResponseEntity<byte[]>(image, new HttpHeaders(), HttpStatus.OK);
            }

    }

    public int getUserCommentsCount(int id) {
        return commentService.getUserCommentsCount(id);
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isLoggedUserAdmin")
    public boolean populateIsLoggedUserAdmin(HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(user.getRole().getRoleName().equals("admin")) {
                return true;
            }
        } catch (AuthenticationFailureException e) {
            return false;
        }
        return false;
    }

    @ModelAttribute("getUserId")
    public int populateGetUser(HttpSession session) {
        return authenticationHelper.tryGetUserId(session);
    }
}
