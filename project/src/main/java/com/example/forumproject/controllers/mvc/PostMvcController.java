package com.example.forumproject.controllers.mvc;

import com.example.forumproject.controllers.rest.AuthenticationHelper;
import com.example.forumproject.exceptions.AuthenticationFailureException;
import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.*;
import com.example.forumproject.models.dtos.*;
import com.example.forumproject.services.contracts.CategoryService;
import com.example.forumproject.services.contracts.CommentService;
import com.example.forumproject.services.contracts.PostService;
import com.example.forumproject.services.contracts.UserService;
import com.example.forumproject.utils.CommentMapper;
import com.example.forumproject.utils.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/posts")
public class PostMvcController {
    private final PostService postService;
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;
    private final CategoryService categoryService;
    private final CommentService commentService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PostMvcController(PostService postService, PostMapper postMapper, CommentMapper commentMapper,
                             CategoryService categoryService, CommentService commentService, UserService userService, AuthenticationHelper authenticationHelper) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.commentMapper = commentMapper;
        this.categoryService = categoryService;
        this.commentService = commentService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showAllPosts(Model model) {
        List <Post> posts =  postService.getAll();
        List <Category> categories = categoryService.getAll();

        model.addAttribute("posts", posts);
        model.addAttribute("categoryTitle","All posts");
        model.addAttribute("newPost",  new PostCreationMvcDto());
        model.addAttribute("categories", categories);
        model.addAttribute("filterPostDto", new FilterPostDto());
        return "posts";
    }

    @GetMapping("/{id}")
    public String showSinglePost(@PathVariable int id, Model model, HttpSession session) {
        //TODO
        User user = new User();
        try {
            boolean isAdminOrOwner = false;
            Post existingPost = postService.getById(id);
            List<Comment> comments = commentService.getCommentsByPostId(id);
            if(populateIsAuthenticated(session)) {
                user = userService.getByUsername((String) session.getAttribute("currentUser"));
                isAdminOrOwner = authenticationHelper.verifyPostOwnershipOrAdmin(user, existingPost);
            }
            List<String> tags = new ArrayList<>();
            existingPost.getTags().forEach(tag -> tags.add(tag.getTagName()));
            //TODO user is hardcoded
            model.addAttribute("user", user);
            model.addAttribute("post", existingPost);
            model.addAttribute("comments", comments);
            model.addAttribute("relatedPosts", postService.getRelatedPosts(tags, id));
            model.addAttribute("categories", categoryService.getAll());
            model.addAttribute("newPost",  new PostCreationMvcDto());
            model.addAttribute("newComment", new CommentDTO());
            model.addAttribute("isOwnerOrAdmin", isAdminOrOwner);
            return "post";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{postId}/comments")
    public String createCommentInPost(@Valid @ModelAttribute("newComment")CommentDTO commentDto, BindingResult errors,
                                      @PathVariable int postId, Model model, HttpSession session){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        boolean isAdminOrOwner = false;
        Post existingPost = postService.getById(postId);
        List<Comment> comments = commentService.getCommentsByPostId(postId);
        List<String> tags = new ArrayList<>();
        existingPost.getTags().forEach(tag -> tags.add(tag.getTagName()));
        if(populateIsAuthenticated(session)) {
            user = userService.getByUsername((String) session.getAttribute("currentUser"));
            isAdminOrOwner = authenticationHelper.verifyPostOwnershipOrAdmin(user, existingPost);
        }
        if (errors.hasErrors()) {
            model.addAttribute("user", user);
            model.addAttribute("post", existingPost);
            model.addAttribute("comments", comments);
            model.addAttribute("relatedPosts", postService.getRelatedPosts(tags, postId));
            model.addAttribute("categories", categoryService.getAll());
            model.addAttribute("newPost",  new PostCreationMvcDto());
            model.addAttribute("isOwnerOrAdmin", isAdminOrOwner);
            return "post";
        }

        try {
            Comment comment = commentMapper.dtoToObject(commentDto, postId, user);
            commentService.createComment(comment, user);
            return "redirect:/posts/" + postId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("user", user);
            model.addAttribute("post", existingPost);
            model.addAttribute("comments", comments);
            model.addAttribute("relatedPosts", postService.getRelatedPosts(tags, postId));
            model.addAttribute("categories", categoryService.getAll());
            model.addAttribute("newPost",  new PostCreationMvcDto());
            model.addAttribute("isOwnerOrAdmin", isAdminOrOwner);
            errors.rejectValue("content", "content_error", e.getMessage());
            return "post";
        }
    }

    @PostMapping
    public String createPost(@Valid @ModelAttribute("newPost") PostCreationMvcDto postCreationMvcDto,
                             BindingResult errors, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            model.addAttribute("filterPostDto", new FilterPostDto());
            return "posts";
        }

        try {
            Post post = postMapper.mvcDtoToObject(postCreationMvcDto, user);
            postService.create(post);
            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            //errors.rejectValue("content", "content_error", e.getMessage());
            return "redirect:/posts";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditPostPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Post post = postService.getById(id);
            PostOutDTO postOutDTO = postMapper.objectToDTO(post);
            model.addAttribute("categoryId", post.getCategory().getId());
            model.addAttribute("newPost", postOutDTO);
            model.addAttribute("categories", categoryService.getAll());
            return "edit-post";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updatePost(@PathVariable int id,@Valid @ModelAttribute("newPost") PostDTO postDTO, HttpSession session,
                             BindingResult bindingResult, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            if (bindingResult.hasErrors()) {
                return "edit-post";
            }
            Post post =  postMapper.updateDTOToObject(postDTO,id);
            postService.update(post,user);
            return "redirect:/posts/" + post.getId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{postId}/comments/{commentId}/update")
    public String showEditCommentPage(@PathVariable int postId, @PathVariable int commentId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Comment comment = commentService.getById(commentId);
            CommentOutDTO commentOutDTO = commentMapper.objectToDto(comment);
            model.addAttribute("newComment", commentOutDTO);

            if(user.getRole().getRoleName().equals("user") && !comment.getUser().getUsername().equals(user.getUsername())) {
                throw new UnauthorizedOperationException("You are unauthorized");
            }

            return "edit-comment";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
    @PostMapping("/{postId}/comments/{commentId}/update")
    public String updateComment(@PathVariable int postId, @PathVariable int commentId, @Valid @ModelAttribute("newComment") CommentDTO commentDTO, HttpSession session,
                             BindingResult bindingResult, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            if (bindingResult.hasErrors()) {
                return "edit-comment";
            }
            Comment comment = commentMapper.updateDTOToObject(commentDTO,commentId);
            commentService.updateComment(comment,user);
            return "redirect:/posts/" + postId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/delete")
    public String deletePost(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            postService.delete(id,user);
            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
//        catch (UnauthorizedOperationException e) {
//            model.addAttribute("error", e.getMessage());
//            return "access-denied";
//        }
    }
    @GetMapping("/{postId}/comments/{commentId}/delete")
    public String deleteComment(@PathVariable int postId, @PathVariable int commentId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Comment comment = commentService.getById(commentId);

            if(user.getRole().getRoleName().equals("user") && !comment.getUser().getUsername().equals(user.getUsername())) {
                throw new UnauthorizedOperationException("You are unauthorized");
            }

            commentService.deleteComment(commentId, user);
            return "redirect:/posts/" + postId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }  catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/filter")
    public String filterPosts(@ModelAttribute("filterPostDto") FilterPostDto filterPostDto,
                              Model model, HttpSession session) {
        var filteredPosts = postService.filter(
                Optional.ofNullable(filterPostDto.getTitle().isBlank() ? null : filterPostDto.getTitle()),
                Optional.ofNullable(filterPostDto.getUsername().isBlank() ? null : filterPostDto.getUsername()),
                Optional.ofNullable(filterPostDto.getContent().isBlank() ? null : filterPostDto.getContent()),
                Optional.ofNullable(filterPostDto.getTagName().isBlank() ? null : filterPostDto.getTagName()),
                Optional.ofNullable(filterPostDto.getSort().isBlank() ? null : filterPostDto.getSort())
        );

        model.addAttribute("posts", filteredPosts);
        List <Category> categories = categoryService.getAll();
        model.addAttribute("categoryTitle","All posts");
        model.addAttribute("newPost",  new PostCreationMvcDto());
        model.addAttribute("categories", categories);

        return "posts";
    }

    @PostMapping("/{id}/votes")
    public String votePost(@PathVariable int id, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if(user.getIsBlocked() || user.isDeleted()) {
            return "redirect:/posts/{id}";
        }

        postService.votePost(user, id);
        return "redirect:/posts/{id}";

    }

    @PostMapping("/{postId}/comments/{commentId}/votes")
    public String voteComment(@PathVariable int postId, @PathVariable int commentId, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if(user.getIsBlocked() || user.isDeleted()) {
            return "redirect:/posts/{postId}";
        }

        commentService.voteComment(user, commentId);
        return "redirect:/posts/{postId}";
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("getUserId")
    public int populateGetUser(HttpSession session) {
        return authenticationHelper.tryGetUserId(session);
    }

    @ModelAttribute("isLoggedUserABlocked")
    public boolean populateIsLoggedUserAdmin(HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(user.getIsBlocked()) {
                return true;
            }
        } catch (AuthenticationFailureException e) {
            return false;
        }
        return false;
    }
}
