package com.example.forumproject.controllers.rest;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.Role;
import com.example.forumproject.models.User;
import com.example.forumproject.models.dtos.RoleDTO;
import com.example.forumproject.services.contracts.RoleService;
import com.example.forumproject.utils.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/roles")
public class RoleController {
    private final RoleService service;
    private final AuthenticationHelper authenticationHelper;
    private final RoleMapper roleMapper;

    @Autowired
    public RoleController(RoleService service, AuthenticationHelper authenticationHelper, RoleMapper roleMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.roleMapper = roleMapper;
    }

    @GetMapping
    public List<Role> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Role getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Role create(@RequestHeader HttpHeaders headers, @Valid @RequestBody RoleDTO roleDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Role role = roleMapper.fromDto(roleDTO);
            service.create(role, user);
            return role;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public Role update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody RoleDTO roleDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Role role = roleMapper.fromDto(roleDTO, id);
            service.update(role, user);
            return role;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
