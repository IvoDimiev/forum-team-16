package com.example.forumproject.controllers.mvc;

import com.example.forumproject.controllers.rest.AuthenticationHelper;
import com.example.forumproject.models.Category;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.User;
import com.example.forumproject.models.dtos.*;
import com.example.forumproject.services.contracts.CategoryService;
import com.example.forumproject.services.contracts.PostService;
import com.example.forumproject.utils.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/categories")
public class CategoryMvcController {
    private final CategoryService categoryService;
    private final PostService postService;
    private final CategoryMapper categoryMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CategoryMvcController(CategoryService categoryService, PostService postService, CategoryMapper categoryMapper, AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.postService = postService;
        this.categoryMapper = categoryMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showAllCategories(Model model) {
        List<Category> categories = categoryService.getAll();
        List<CategoryOutDto> categoryOutDtos = new ArrayList<>();

        categories.forEach(category -> categoryOutDtos.add(categoryMapper.objectToDto(category)));
        model.addAttribute("categories", categoryOutDtos);
        model.addAttribute("newPost", new PostCreationMvcDto());
        return "categories";
    }

    @GetMapping("/{id}")
    public String showSingleCategory(@PathVariable int id, Model model) {
        List<Post> relatedPostsDTOs =  postService.getPostsByCategory(id);
        Category category = categoryService.getById(id);

        model.addAttribute("posts", relatedPostsDTOs);
        model.addAttribute("categoryTitle", category.getName());
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("newPost", new PostCreationMvcDto());
        return "category-posts";
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("getUserId")
    public int populateGetUser(HttpSession session) {
        return authenticationHelper.tryGetUserId(session);
    }

}
