package com.example.forumproject.controllers.rest;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.PhoneNumber;
import com.example.forumproject.models.Role;
import com.example.forumproject.models.User;
import com.example.forumproject.models.dtos.*;
import com.example.forumproject.services.contracts.PhoneNumberService;
import com.example.forumproject.services.contracts.RoleService;
import com.example.forumproject.services.contracts.UserService;
import com.example.forumproject.utils.PhoneNumberMapper;
import com.example.forumproject.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;
    private final RoleService roleService;
    private final UserMapper userMapper;
    private final PhoneNumberService phoneNumberService;
    private final PhoneNumberMapper phoneNumberMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService, RoleService roleService, UserMapper userMapper,
                          PhoneNumberService phoneNumberService, AuthenticationHelper authenticationHelper,
                          PhoneNumberMapper phoneNumberMapper) {
        this.userService = userService;
        this.roleService = roleService;
        this.userMapper = userMapper;
        this.phoneNumberService = phoneNumberService;
        this.authenticationHelper = authenticationHelper;
        this.phoneNumberMapper = phoneNumberMapper;
    }

    @GetMapping
    public List<UserOutDTO> getAll() {
        List<User> users = userService.getAll();
        return users.stream().map(userMapper::objectToDTO).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public UserOutDTO getById(@PathVariable int id) {
        try {
            return userMapper.objectToDTO(userService.getById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<UserOutDTO> filter(@RequestParam(required = false) Optional<String> username,
                                   @RequestParam(required = false) Optional<String> email,
                                   @RequestParam(required = false) Optional<String> firstName,
                                   @RequestParam(required = false) Optional<String> lastName,
                                   @RequestParam(required = false) Optional<String> sort) {
        try {
            List<User> users = userService.filter(email, username, firstName, lastName, sort);
            return users.stream().map(userMapper::objectToDTO).collect(Collectors.toList());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    public UserOutDTO create(@Valid @RequestBody RegisterUserDTO registerUserDTO) {
        try {
            User user = userMapper.registerDTOToObject(registerUserDTO);
            userService.create(user);
            return userMapper.objectToDTO(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/{id}/phone-number")
    public PhoneNumber createPhoneNumber(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody PhoneNumberDTO phoneNumberDTO) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            PhoneNumber phoneToCreate = phoneNumberMapper.dtoToObject(phoneNumberDTO);
            userService.createPhoneNumber(executingUser, id, phoneToCreate);
            return phoneToCreate;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public UserOutDTO update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserDTO userDTO) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            User userToUpdate = userMapper.dtoToObject(userDTO, id);
            userService.update(executingUser, userToUpdate);
            return userMapper.objectToDTO(userToUpdate);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/phone-number")
    public PhoneNumber updatePhoneNumber(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody PhoneNumberDTO phoneNumberDTO) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            PhoneNumber newPhone = phoneNumberMapper.dtoToObject(phoneNumberDTO);
            userService.updatePhoneNumber(executingUser, id, newPhone);
            return phoneNumberService.getByPhoneNumber(newPhone.getPhoneNumber());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/roles")
    public UserOutDTO updateRole(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                 @Valid @RequestBody RoleDTO roleDTO) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            Role roleToUpdate = roleService.getByName(roleDTO.getRoleName());
            User userToBeUpdated = userService.updateRole(executingUser, id, roleToUpdate);
            return userMapper.objectToDTO(userToBeUpdated);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
        throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/status")
    public UserOutDTO updateBlocked(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            User userToBeUpdated = userService.updateBlocked(executingUser, id);
            return userMapper.objectToDTO(userToBeUpdated);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            userService.delete(executingUser, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}/phone-number")
    public PhoneNumber deletePhoneNumber(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            return userService.deletePhoneNumber(executingUser, id);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
