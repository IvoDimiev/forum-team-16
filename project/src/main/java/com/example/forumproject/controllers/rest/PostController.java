package com.example.forumproject.controllers.rest;


import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.models.*;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.dtos.*;
import com.example.forumproject.services.contracts.CommentService;
import com.example.forumproject.services.contracts.PostService;
import com.example.forumproject.services.contracts.TagService;
import com.example.forumproject.utils.CategoryMapper;
import com.example.forumproject.utils.CommentMapper;
import com.example.forumproject.utils.PostMapper;
import com.example.forumproject.utils.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/posts")
public class PostController {
    private final PostService postService;
    private final PostMapper postMapper;
    private final TagService tagService;
    private final TagMapper tagMapper;
    private final AuthenticationHelper authenticationHelper;
    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final CategoryMapper categoryMapper;

    @Autowired
    public PostController(PostService postService, PostMapper postMapper, TagService tagService, TagMapper tagMapper,
                          AuthenticationHelper authenticationHelper, CommentService commentService,
                          CommentMapper commentMapper, CategoryMapper categoryMapper) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.tagService = tagService;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.categoryMapper = categoryMapper;
    }

    @GetMapping
    public List<PostOutDTO> getAll(@RequestParam(required = false) Optional<String> search) {
        List<Post> posts = postService.search(search);
        List<PostOutDTO> result = new ArrayList<>();
        posts.forEach(post -> result.add(postMapper.objectToDTO(post)));
        return result;
    }

    @GetMapping("/{postId}/comments")
    public Set<CommentOutDTO> getComments(@PathVariable int postId) {
        try {
            Post post = postService.getById(postId);
            Set<Comment> comments = post.getComments();
            return comments.stream().map(commentMapper::objectToDto).collect(Collectors.toSet());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/10-most-commented")
    public List<PostOutDTO> getMostCommentedPosts() {
        try {
            List<Post> posts = postService.getMostCommentedPosts();
            List<PostOutDTO> result = new ArrayList<>();
            posts.forEach(post -> result.add(postMapper.objectToDTO(post)));
            return result;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/10-most-recent")
    public List<PostOutDTO> getMostRecentlyCreatedPosts() {
        try {
            List<Post> posts = postService.getTenMostRecent();
            List<PostOutDTO> result = new ArrayList<>();
            posts.forEach(post -> result.add(postMapper.objectToDTO(post)));
            return result;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public PostOutDTO getById(@PathVariable int id) {
        try {
            return postMapper.objectToDTO(postService.getById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<PostOutDTO> filter(@RequestParam(required = false) Optional<String> title,
                                   @RequestParam(required = false) Optional<String> username,
                                   @RequestParam(required = false) Optional<String> content,
                                   @RequestParam(required = false) Optional<String> tagName,
                                   @RequestParam(required = false) Optional<String> sort) {
        try {
            List<Post> posts = postService.filter(title, username, content, tagName, sort);
            List<PostOutDTO> result = new ArrayList<>();
            posts.forEach(post -> result.add(postMapper.objectToDTO(post)));
            return result;
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    public PostOutDTO create(@RequestHeader HttpHeaders headers, @Valid @RequestBody PostDTO postDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.dtoToObject(postDTO, user);
            postService.create(post);
            return postMapper.objectToDTO(post);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{postId}/comments")
    public CommentOutDTO createComment(@RequestHeader HttpHeaders headers, @PathVariable int postId, @Valid @RequestBody CommentDTO commentDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.dtoToObject(commentDTO, postId, user);
            commentService.createComment(comment, user);
            return commentMapper.objectToDto(comment);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{postId}/tags")
    public Tag createTagInPost(@RequestHeader HttpHeaders headers, @Valid @RequestBody TagDTO tagDTO,
                               @PathVariable int postId) {
        try {
            Tag tag = tagMapper.dtoToObject(tagDTO);
            User user = authenticationHelper.tryGetUser(headers);
            postService.createTagInPost(tag, user, postId);
            return tagService.getByName(tag.getTagName());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{postId}/vote")
    public PostOutDTO votePost(@RequestHeader HttpHeaders headers, @PathVariable int postId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.votePost(user, postId);
            return postMapper.objectToDTO(post);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{postId}/comments/{commentId}/vote")
    public CommentOutDTO voteComment(@RequestHeader HttpHeaders headers, @PathVariable int postId, @PathVariable int commentId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentService.voteComment(user, commentId);
            return commentMapper.objectToDto(comment);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public PostOutDTO update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody PostDTO postDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.updateDTOToObject(postDTO, id);
            postService.update(post, user);
            return postMapper.objectToDTO(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/category")
    public PostOutDTO updatePostCategory(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody CategoryDTO categoryDTO) {
        try {
            User userToUpdateCategory = authenticationHelper.tryGetUser(headers);
            Category newPostCategory = categoryMapper.fromDto(categoryDTO);
            Post post = postService.updatePostCategory(newPostCategory, id, userToUpdateCategory);
            return postMapper.objectToDTO(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{postId}/comments/{commentId}")
    public CommentOutDTO updateComment(@RequestHeader HttpHeaders headers, @PathVariable int postId, @PathVariable int commentId,
                                       @Valid @RequestBody CommentDTO commentDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.updateDTOToObject(commentDTO, commentId);
            commentService.updateComment(comment, user);
            return commentMapper.objectToDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            postService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{postId}/comments/{commentId}")
    public void deleteComment(@RequestHeader HttpHeaders headers, @PathVariable int postId, @PathVariable int commentId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            commentService.deleteComment(commentId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @DeleteMapping("/{postId}/tags")
    public void deleteTagInPost(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                                @Valid @RequestBody TagDTO tagDTO) {
        try {
            Tag tag = tagMapper.dtoToObject(tagDTO);
            User user = authenticationHelper.tryGetUser(headers);
            postService.deleteTagInPost(tag, user, postId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
