package com.example.forumproject.utils;

import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.models.Category;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.Tag;
import com.example.forumproject.models.User;
import com.example.forumproject.models.dtos.PostCreationMvcDto;
import com.example.forumproject.models.dtos.PostDTO;
import com.example.forumproject.models.dtos.PostOutDTO;
import com.example.forumproject.services.contracts.CategoryService;
import com.example.forumproject.services.contracts.PostService;
import com.example.forumproject.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Component
public class PostMapper {

    private final PostService postService;
    private final CategoryService categoryService;
    private final TagService tagService;

    @Autowired
    public PostMapper(PostService postService, CategoryService categoryService, TagService tagService) {
        this.postService = postService;
        this.categoryService = categoryService;
        this.tagService = tagService;
    }

    public Post dtoToObject(PostDTO postDTO, User user) {
        Category category = categoryService.getById(postDTO.getCategoryId());
        Post post = new Post();
        post.setTitle(postDTO.getTitle());
        post.setContent(postDTO.getContent());
        post.setUser(user);
        post.setCategory(category);
        post.setVotes(new HashSet<>());
        post.setComments(new HashSet<>());
        post.setTags(new HashSet<>());

        return post;
    }
    public Post mvcDtoToObject(PostCreationMvcDto postCreationMvcDto, User user) {
        Category category = categoryService.getById(postCreationMvcDto.getCategoryId());
        Post post = new Post();
        post.setTitle(postCreationMvcDto.getTitle());
        post.setContent(postCreationMvcDto.getContent());
        post.setUser(user);
        post.setCategory(category);
        post.setVotes(new HashSet<>());
        post.setComments(new HashSet<>());
        if (postCreationMvcDto.getTags().length() == 0) {
            post.setTags(new HashSet<>());
        } else {
            initializeTagSetInPost(postCreationMvcDto, user, post);
        }
        return post;
    }

    public Post updateDTOToObject(PostDTO postDTO, int id) {
        Post postToBeUpdated = postService.getById(id);
        Category category = categoryService.getById(postDTO.getCategoryId());
        postToBeUpdated.setTitle(postDTO.getTitle());
        postToBeUpdated.setContent(postDTO.getContent());
        postToBeUpdated.setCategory(category);
        return postToBeUpdated;
    }

    public PostOutDTO objectToDTO(Post post) {
        PostOutDTO postOutDTO = new PostOutDTO();
        postOutDTO.setTitle(post.getTitle());
        postOutDTO.setContent(post.getContent());
        postOutDTO.setLikesCount(post.getVotes().size());
        postOutDTO.setCommentsCount(post.getComments().size());
        postOutDTO.setId(post.getId());
        Set<Tag> tagsSet = post.getTags();

        if(!tagsSet.isEmpty()) {
            postOutDTO.setTags(new ArrayList<>(tagsSet));
        } else  {
            postOutDTO.setTags(new ArrayList<>());
        }
        User user = post.getUser();
        if(user.isDeleted()) {
            postOutDTO.setAuthorUsername("DELETED");
        } else {
            postOutDTO.setAuthorUsername(user.getUsername());
        }
        postOutDTO.setCategory(post.getCategory().getName());
        postOutDTO.setCreationDate(post.getCreationDate());
        return postOutDTO;
    }

    private void initializeTagSetInPost(PostCreationMvcDto postCreationMvcDto, User user, Post post) {
        String [] tagNames = postCreationMvcDto.getTags().split(";");
        Set <Tag> postTags = new HashSet<>();
        for (String tag : tagNames) {
            Tag postTag = new Tag();
            try {
                postTag = tagService.getByName(tag);
            } catch (EntityNotFoundException e) {
                postTag.setTagName(tag);
                tagService.create(postTag, user);
            }
            postTags.add(postTag);
        }
        post.setTags(postTags);
    }
}
