package com.example.forumproject.utils;

import com.example.forumproject.models.Category;
import com.example.forumproject.models.Role;
import com.example.forumproject.models.Tag;
import com.example.forumproject.models.dtos.CategoryDTO;
import com.example.forumproject.models.dtos.RoleDTO;
import com.example.forumproject.models.dtos.TagDTO;
import com.example.forumproject.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class RoleMapper {
    private final RoleService roleService;

    @Autowired
    public RoleMapper(RoleService roleService) {
        this.roleService = roleService;
    }

    public Role fromDto(RoleDTO roleDTO) {
        Role role  = new Role();
        role.setRoleName(roleDTO.getRoleName());
        return role;
    }

    public Role fromDto(RoleDTO roleDTO, int existingRoleId) {
        Role roleToBeUpdated = roleService.getById(existingRoleId);
        roleToBeUpdated.setRoleName(roleDTO.getRoleName());
        return roleToBeUpdated;
    }


}
