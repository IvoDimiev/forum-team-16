package com.example.forumproject.utils;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.Comment;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.Tag;
import com.example.forumproject.models.User;

public class ValidationHelpers {
    private static final String ADMIN_ROLE = "admin";
    private static final String UNAUTHORIZED_POST_UPDATE = "Ony admin or post's creator can modify a post.";
    private static final String BLOCKED_USER_ERROR_MESSAGE = "Your account is currently blocked! You cannot create or edit posts.";
    private static final String BLOCKED_USER_ERROR_MESSAGE_FOR_COMMENT = "Your account is currently blocked! You cannot create or edit comments.";
    private static final String DELETE_USER_ERROR_MESSAGE = "Your account is currently deleted! You cannot create or edit posts.";
    private static final String MODIFY_USER_ERROR_MESSAGE = "Only admin or account's owners can modify a user";

    public static void validateUserIsAdmin(User user) {
        if (!user.getRole().getRoleName().equalsIgnoreCase(ADMIN_ROLE)) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_POST_UPDATE);
        }
    }

    public static void validateUserIsAdminOrPostCreator(User user, Post post) {
        if (!user.getRole().getRoleName().equalsIgnoreCase(ADMIN_ROLE)) {
            if ((!post.getUser().getUsername().equals(user.getUsername()))) {
                throw new UnauthorizedOperationException(UNAUTHORIZED_POST_UPDATE);
            }
        }
    }

    public static void validateUserIsAdminOrCommentCreator(User user, Comment comment) {
        if (!user.getRole().getRoleName().equalsIgnoreCase(ADMIN_ROLE)) {
            if ((!comment.getUser().getUsername().equals(user.getUsername()))) {
                throw new UnauthorizedOperationException(UNAUTHORIZED_POST_UPDATE);
            }
        }
    }

    public static void validateUserIsAdminOrAccountOwner(User executingUser, User userToUpdate) {
        if (!executingUser.getRole().getRoleName().equalsIgnoreCase(ADMIN_ROLE)) {
            if ((!executingUser.getUsername().equals(userToUpdate.getUsername()))) {
                throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
            }
        }
    }

    public static void validateUserIsDeleted(User user) {
        if (user.isDeleted()) {
            throw new UnauthorizedOperationException(DELETE_USER_ERROR_MESSAGE);
        }
    }

    public static void validateUserIsBlocked(User user) {
        if (user.getIsBlocked()) {
            throw new UnauthorizedOperationException(BLOCKED_USER_ERROR_MESSAGE);
        }
    }

    public static void validateUserIsBlockedForComment(User user) {
        if (user.getIsBlocked()) {
            throw new UnauthorizedOperationException(BLOCKED_USER_ERROR_MESSAGE_FOR_COMMENT);
        }
    }

    public static void validatePostContainsTag(Post post, Tag tag) {
        if (!post.getTags().contains(tag)) {
            throw new EntityNotFoundException("Tag", "name", tag.getTagName());
        }
    }

    public static void isTagAlreadyInPost(Post post, Tag tag) {
        if (post.getTags().contains(tag)) {
            throw new DuplicateEntityException("Tag", "name", tag.getTagName());
        }
    }
}
