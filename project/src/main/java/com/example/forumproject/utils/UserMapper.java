package com.example.forumproject.utils;

import com.example.forumproject.models.User;
import com.example.forumproject.models.dtos.RegisterUserDTO;
import com.example.forumproject.models.dtos.UserDTO;
import com.example.forumproject.models.dtos.UserOutDTO;
import com.example.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private final UserService userService;

    @Autowired
    public UserMapper(UserService userService) {
        this.userService = userService;
    }

    public User dtoToObject(UserDTO userDTO, int id) {
        User user = userService.getById(id);
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
//        user.setPassword(userDTO.getPassword());
        return user;
    }

    public User registerDTOToObject(RegisterUserDTO registerUserDTO) {
        User user = new User();
        user.setFirstName(registerUserDTO.getFirstName());
        user.setLastName(registerUserDTO.getLastName());
        user.setUsername(registerUserDTO.getUsername());
        user.setEmail(registerUserDTO.getEmail());
        user.setPassword(registerUserDTO.getPassword());
        return user;
    }

    public UserOutDTO objectToDTO(User user) {
        UserOutDTO userOutDTO = new UserOutDTO();
        userOutDTO.setFirstName(user.getFirstName());
        userOutDTO.setLastName(user.getLastName());
        userOutDTO.setUsername(user.getUsername());
        userOutDTO.setEmail(user.getEmail());
        userOutDTO.setRoleName(user.getRole().getRoleName());
        userOutDTO.setCreationDate(user.getCreationDate());
        userOutDTO.setBlocked(user.getIsBlocked());
        return userOutDTO;
    }
}
