package com.example.forumproject.utils;

import com.example.forumproject.models.Category;
import com.example.forumproject.models.dtos.CategoryDTO;
import com.example.forumproject.models.dtos.CategoryOutDto;
import com.example.forumproject.services.contracts.CategoryService;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {
    private final CategoryService categoryService;

    public CategoryMapper(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public Category fromDto(CategoryDTO categoryDTO) {
        Category category = new Category();
        category.setName(categoryDTO.getName());
        return category;
    }

    public Category fromDto(CategoryDTO categoryDTO, int existingCategoryId) {
        Category categoryToBeUpdated = categoryService.getById(existingCategoryId);
        categoryToBeUpdated.setName(categoryDTO.getName());
        return categoryToBeUpdated;
    }

    public CategoryOutDto objectToDto(Category category) {
        CategoryOutDto categoryOutDto = new CategoryOutDto();
        categoryOutDto.setId(category.getId());
        categoryOutDto.setName(category.getName());
        categoryOutDto.setNumberOfPosts(categoryService.getPostsCountInCategory(category.getId()));
        return categoryOutDto;
    }
}
