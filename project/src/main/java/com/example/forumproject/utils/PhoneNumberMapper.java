package com.example.forumproject.utils;

import com.example.forumproject.models.PhoneNumber;
import com.example.forumproject.models.User;
import com.example.forumproject.models.dtos.PhoneNumberDTO;
import com.example.forumproject.services.contracts.PhoneNumberService;
import com.example.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PhoneNumberMapper {
    private final PhoneNumberService phoneNumberService;
    private final UserService userService;

    @Autowired
    public PhoneNumberMapper(PhoneNumberService phoneNumberService, UserService userService) {
        this.phoneNumberService = phoneNumberService;
        this.userService = userService;
    }

    public PhoneNumber dtoToObject(PhoneNumberDTO phoneNumberDTO) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhoneNumber(phoneNumberDTO.getPhoneNumber());
        return phoneNumber;
    }
}
