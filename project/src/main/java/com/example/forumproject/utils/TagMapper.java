package com.example.forumproject.utils;

import com.example.forumproject.models.Tag;
import com.example.forumproject.models.dtos.TagDTO;
import com.example.forumproject.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class TagMapper {
    private final TagService tagService;

    @Autowired
    public TagMapper(TagService tagService) {
        this.tagService = tagService;
    }

    public Tag dtoToObject(TagDTO tagDTO) {
        Tag tag = new Tag();
        tag.setTagName(tagDTO.getTagName().toLowerCase(Locale.ROOT));
        return tag;
    }

    public Tag dtoToObject(TagDTO tagDTO, int existingTagId) {
        Tag tagToBeUpdated = tagService.getById(existingTagId);
        tagToBeUpdated.setTagName(tagDTO.getTagName().toLowerCase(Locale.ROOT));
        return tagToBeUpdated;
    }

}
