package com.example.forumproject.utils;

import com.example.forumproject.models.Comment;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.User;
import com.example.forumproject.models.dtos.CommentDTO;
import com.example.forumproject.models.dtos.CommentOutDTO;
import com.example.forumproject.services.contracts.CommentService;
import com.example.forumproject.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class CommentMapper {
    private final PostService postService;
    private final CommentService commentService;

    @Autowired
    public CommentMapper(PostService postService, CommentService commentService) {
        this.postService = postService;
        this.commentService = commentService;
    }

    public Comment dtoToObject(CommentDTO commentDTO, int id, User user) {
        Comment comment = new Comment();
        Post post = postService.getById(id);
        comment.setPost(post);
        comment.setContent(commentDTO.getContent());
        comment.setUser(user);
        comment.setVotes(new HashSet<>());
        return comment;
    }

    public Comment updateDTOToObject(CommentDTO commentDTO, int commentId) {
        Comment comment = commentService.getById(commentId);
        comment.setContent(commentDTO.getContent());
        return comment;
    }

    public CommentOutDTO objectToDto(Comment comment) {
        CommentOutDTO commentOutDTO = new CommentOutDTO();
        commentOutDTO.setId(comment.getId());
        commentOutDTO.setContent(comment.getContent());
        commentOutDTO.setCreationDate(comment.getCreationDate());
        commentOutDTO.setAuthorUsername(comment.getUser().getUsername());
        commentOutDTO.setLikesCount(comment.getVotes().size());
        return commentOutDTO;
    }
}
