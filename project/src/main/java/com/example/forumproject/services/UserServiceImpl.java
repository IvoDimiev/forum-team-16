package com.example.forumproject.services;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.PhoneNumber;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.Role;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.PhoneNumberRepository;
import com.example.forumproject.repositories.contracts.UserRepository;
import com.example.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;

import static com.example.forumproject.utils.ValidationHelpers.validateUserIsAdmin;
import static com.example.forumproject.utils.ValidationHelpers.validateUserIsAdminOrAccountOwner;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PhoneNumberRepository phoneNumberRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PhoneNumberRepository phoneNumberRepository) {
        this.userRepository = userRepository;
        this.phoneNumberRepository = phoneNumberRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public void create(User user) {
        checkIfUserIsUnique(user);
        userRepository.create(user);
    }

    @Override
    public void createPhoneNumber(User executingUser, int id, PhoneNumber phoneToCreate) {
        User userToGetPhone = getById(id);
        validateUserIsAdmin(executingUser);
        validateUserIsAdmin(userToGetPhone);
        boolean duplicateExists = true;
        try {
            phoneNumberRepository.getByUserId(userToGetPhone.getId());
        } catch (EntityNotFoundException e) {
            duplicateExists=false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("This user is already", "phone number", userToGetPhone.getPhone().getPhoneNumber());
        }
        checkIfPhoneExists(phoneToCreate);
        phoneToCreate.setUserId(userToGetPhone.getId());
        phoneNumberRepository.create(phoneToCreate);
    }

    @Override
    public void update(User executingUser, User userToUpdate) {
        validateUserIsAdminOrAccountOwner(executingUser, userToUpdate);
        boolean duplicateExists = true;
        try {
            User existingUser = userRepository.getByEmail(userToUpdate.getEmail());
            if (existingUser.getId() == userToUpdate.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", userToUpdate.getEmail());
        }
        userRepository.update(userToUpdate);
    }

    @Override
    public User updateRole(User executingUser, int id, Role roleToUpdate) {
        User userToGetRole = getById(id);
        validateUserIsAdmin(executingUser);
        if(userToGetRole.getRole().equals(roleToUpdate)){
            throw new DuplicateEntityException("User", "role", roleToUpdate.getRoleName());
        }
        userToGetRole.setRole(roleToUpdate);
        userRepository.update(userToGetRole);
        return userToGetRole;
    }

    @Override
    public User updateBlocked(User executingUser, int id) {
        User userToBeUpdated = getById(id);
        validateUserIsAdmin(executingUser);
        boolean newBlockedStatus = !userToBeUpdated.getIsBlocked();
        userToBeUpdated.setBlocked(newBlockedStatus);
        userRepository.update(userToBeUpdated);
        return userToBeUpdated;
    }

    @Override
    public void updatePhoneNumber(User executingUser, int id, PhoneNumber newPhone) {
        User userToGetPhone = getById(id);
        validateUserIsAdmin(executingUser);
        validateUserIsAdmin(userToGetPhone);
        PhoneNumber oldPhone = phoneNumberRepository.getByUserId(userToGetPhone.getId());
        checkIfPhoneExists(newPhone);
        oldPhone.setPhoneNumber(newPhone.getPhoneNumber());
        phoneNumberRepository.update(oldPhone);
    }

    @Override
    public List<User> filter(Optional<String> username, Optional<String> email,  Optional<String> firstName, Optional<String> lastName, Optional<String> sort) {
        return userRepository.filter(username, email, firstName, lastName, sort);
    }

    @Override
    public PhoneNumber deletePhoneNumber(User executingUser, int userToBeUpdatedId) {
        User userToGetPhoneDeleted = getById(userToBeUpdatedId);
        validateUserIsAdminOrAccountOwner(executingUser, userToGetPhoneDeleted);
        validateUserIsAdmin(userToGetPhoneDeleted);
        phoneNumberRepository.delete(userToGetPhoneDeleted.getPhone());
        return userToGetPhoneDeleted.getPhone();
    }

    @Override
    public void changePassword(User user, String oldPassword, String newPassword, String confirmedNewPassword) {
        if(!user.getPassword().equals(oldPassword)) {
            throw new UnauthorizedOperationException("Your old password is incorrect");
        }

        if(!newPassword.equals(confirmedNewPassword)) {
            throw new InputMismatchException("New password do not match with confirmed one");
        }
    }

    @Override
    public void delete(User executingUser, int id) {
        User userToBeDeleted = getById(id);
        validateUserIsAdminOrAccountOwner(executingUser, userToBeDeleted);
        userToBeDeleted.setDeleted(true);
        userRepository.update(userToBeDeleted);
    }

    public void checkIfUserIsUnique(User user) {
        boolean duplicateExists = true;
        try {
            userRepository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        duplicateExists = true;

        try {
            userRepository.getByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
    }

    private void checkIfPhoneExists(PhoneNumber phoneToCreate) {
        boolean duplicateExists = true;
        try {
            phoneNumberRepository.getByPhoneNumber(phoneToCreate.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "phone-number", phoneToCreate.getPhoneNumber());
        }
    }
}