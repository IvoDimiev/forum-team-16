package com.example.forumproject.services;

import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.models.Image;
import com.example.forumproject.repositories.contracts.ImageRepository;
import com.example.forumproject.services.contracts.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageServiceImp implements ImageService {
    private final ImageRepository repository;

    @Autowired
    public ImageServiceImp(ImageRepository repository) {
        this.repository = repository;
    }

    @Override
    public Image getImage(int userId) {
        return repository.getImage(userId);
    }

    @Override
    public void saveImage(int userId, byte[] image) {
        Image newImage = new Image();
        newImage.setImage(image);
        newImage.setUserId(userId);

        try {
            repository.getImage(userId);
            update(newImage);
        } catch (EntityNotFoundException e) {
            repository.create(newImage);
        }
    }

    @Override
    public void update(Image image) {
      repository.update(image);
    }
}
