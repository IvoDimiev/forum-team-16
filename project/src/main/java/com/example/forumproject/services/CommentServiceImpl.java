package com.example.forumproject.services;

import com.example.forumproject.models.Comment;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.CommentRepository;
import com.example.forumproject.repositories.contracts.PostRepository;
import com.example.forumproject.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.forumproject.utils.ValidationHelpers.*;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository repository;
    private final PostRepository postRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository repository, PostRepository postRepository) {
        this.repository = repository;
        this.postRepository = postRepository;
    }

    @Override
    public List<Comment> getAll() {
        return repository.getAll();
    }

    @Override
    public Comment getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void createComment(Comment comment, User user) {
        validateUserIsBlockedForComment(user);
        validateUserIsDeleted(user);
        repository.createComment(comment);
    }

    @Override
    public Comment voteComment(User user, int commentId) {
        Comment comment = getById(commentId);
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        boolean voted = comment.getVotes().contains(user);
        if (voted) {
            comment.getVotes().remove(user);
        } else {
            comment.addVote(user);
        }
        repository.updateComment(comment);
        return comment;
    }

    @Override
    public void updateComment(Comment comment, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsAdminOrCommentCreator(user, comment);
        repository.updateComment(comment);
    }

    @Override
    public void deleteComment(int commentId, User user) {
        Comment comment = getById(commentId);
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsAdminOrCommentCreator(user, comment);
        comment.getVotes().clear();
        repository.deleteComment(comment);
    }

    @Override
    public int getUserCommentsCount(int id) {
        // think about alternative (with filter)
        List<Comment> userComments = getAll().stream()
                .filter(comment -> comment.getUser().getId() == id)
                .collect(Collectors.toList());
        return userComments.size();
    }

    @Override
    public List<Comment> getCommentsByPostId(int postId) {
        return repository.getCommentsByPostId(postId);
    }


}
