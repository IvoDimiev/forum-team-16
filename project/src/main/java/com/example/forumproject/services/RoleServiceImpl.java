package com.example.forumproject.services;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.models.Role;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.RoleRepository;
import com.example.forumproject.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.forumproject.utils.ValidationHelpers.*;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository repository;

    @Autowired
    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Role> getAll() {
        return repository.getAll();
    }

    @Override
    public Role getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Role getByName(String name) {
        return repository.getByName(name);
    }

    @Override
    public void create(Role role, User user) {
        validateUserIsAdmin(user);
        validateUserIsDeleted(user);
        validateUserIsBlocked(user);
        boolean duplicateExists = true;
        try {
            repository.getByName(role.getRoleName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Role", "name", role.getRoleName());
        }
        repository.create(role);
    }

    @Override
    public void update(Role role, User user) {
        validateUserIsAdmin(user);
        validateUserIsDeleted(user);
        validateUserIsBlocked(user);
        boolean duplicateExists = true;
        try {
            repository.getByName(role.getRoleName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Role", "name", role.getRoleName());
        }
        repository.update(role);
    }
}
