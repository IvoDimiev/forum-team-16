package com.example.forumproject.services;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.Tag;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.TagRepository;
import com.example.forumproject.services.contracts.PostService;
import com.example.forumproject.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.forumproject.utils.ValidationHelpers.*;

@Service
public class TagServiceImpl implements TagService {
    private final TagRepository tagRepository;
    private final PostService postService;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, PostService postService) {
        this.tagRepository = tagRepository;
        this.postService = postService;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }

    @Override
    public Tag getById(int id) {
        return tagRepository.getById(id);
    }

    @Override
    public Tag getByName(String tagName) {
        return tagRepository.getByName(tagName);
    }

    @Override
    public void create(Tag tag, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        boolean duplicateExists = true;
        try {
            tagRepository.getByName(tag.getTagName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Tag", "name", tag.getTagName());
        }
        tagRepository.create(tag);
    }

    @Override
    public void update(Tag tag, User user) {
        validateUserIsAdmin(user);
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        boolean duplicateExists = true;
        try {
            tagRepository.getByName(tag.getTagName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Tag", "name", tag.getTagName());
        }
        tagRepository.update(tag);
    }

    @Override
    public void delete(int id, User user) {
        Tag tag = getById(id);
        validateUserIsAdmin(user);
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);

        List<Post>posts = postService.getAll();
        List<Post> postsToUpdate = posts.stream().filter(post -> post.getTags().contains(tag)).collect(Collectors.toList());
        postsToUpdate.forEach(post -> post.getTags().remove(tag));
        postsToUpdate.forEach(post -> postService.update(post,user));
        tagRepository.delete(tag);
    }

}
