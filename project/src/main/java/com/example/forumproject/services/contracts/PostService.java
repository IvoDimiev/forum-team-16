package com.example.forumproject.services.contracts;

import com.example.forumproject.models.Category;
import com.example.forumproject.models.Post;
import com.example.forumproject.models.Tag;
import com.example.forumproject.models.User;

import java.util.List;
import java.util.Optional;

public interface PostService {
    List<Post> getAll();

    List<Post> search(Optional<String> search);

    List<Post> filter(Optional<String> title, Optional<String> username, Optional<String> content, Optional<String> tagName,
                      Optional<String> sort);

    List<Post> getMostCommentedPosts();

    List<Post> getTenMostRecent();

    List<Post> getPostsByCategory(int id);

    Post getById(int id);

    void create(Post post);

    void createTagInPost(Tag tag, User user, int postId);

    Post votePost(User user, int postId);

    void update(Post post, User user);

    Post updatePostCategory(Category category, int id, User userToUpdateCategory);

    void delete(int postId, User user);

    void deleteTagInPost(Tag tag, User user, int postId);

    List<Post> getRelatedPosts(List<String> tags, int id);
}
