package com.example.forumproject.services.contracts;

import com.example.forumproject.models.Tag;
import com.example.forumproject.models.User;

import java.util.List;

public interface TagService {
    List<Tag> getAll();

    Tag getById(int id);

    Tag getByName(String tagName);

    void create(Tag tag, User user);

    void update(Tag tag, User user);

    void delete(int id, User user);

}
