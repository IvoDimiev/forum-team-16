package com.example.forumproject.services.contracts;

import com.example.forumproject.models.Role;
import com.example.forumproject.models.User;

import java.util.List;

public interface RoleService {

    List<Role> getAll();

    Role getById(int id);

    Role getByName(String name);

    void create(Role role, User user);

    void update(Role role, User user);

}
