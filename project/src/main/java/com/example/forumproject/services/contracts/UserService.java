package com.example.forumproject.services.contracts;

import com.example.forumproject.models.PhoneNumber;
import com.example.forumproject.models.Role;
import com.example.forumproject.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    List<User> filter(Optional<String> email, Optional<String> username, Optional<String> firstName, Optional<String> lastName, Optional<String> sort);

    void create(User user);

    void createPhoneNumber(User executingUser, int id, PhoneNumber phoneToCreate);

    void update(User executingUser, User userToUpdate);

    User updateRole(User executingUser, int id, Role roleToUpdate);

    User updateBlocked(User executingUser, int id);

    void updatePhoneNumber(User executingUser, int id, PhoneNumber newPhone);

    void delete(User executingUser, int id);

    PhoneNumber deletePhoneNumber(User executingUser, int userToBeUpdatedId);

    void changePassword(User user, String oldPassword, String newPassword, String confirmedNewPassword);
}
