package com.example.forumproject.services.contracts;

import com.example.forumproject.models.PhoneNumber;

import java.util.List;

public interface PhoneNumberService {

    PhoneNumber getByPhoneNumber(String number);

    PhoneNumber getByUserId(int id);

}
