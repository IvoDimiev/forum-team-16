package com.example.forumproject.services.contracts;

import com.example.forumproject.models.Comment;
import com.example.forumproject.models.User;

import java.util.List;

public interface CommentService {

    List<Comment> getAll();

    Comment getById(int id);

    void createComment(Comment comment, User user);

    void updateComment(Comment comment, User user);

    Comment voteComment(User user, int commentId);

    void deleteComment(int commentId, User user);

    int getUserCommentsCount(int id);

    List<Comment> getCommentsByPostId(int postId);
}
