package com.example.forumproject.services;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.models.PhoneNumber;
import com.example.forumproject.repositories.contracts.PhoneNumberRepository;
import com.example.forumproject.services.contracts.PhoneNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhoneNumberServiceImpl implements PhoneNumberService {
    private final PhoneNumberRepository repository;

    @Autowired
    public PhoneNumberServiceImpl(PhoneNumberRepository repository) {
        this.repository = repository;
    }

    @Override
    public PhoneNumber getByPhoneNumber(String number) {
        return repository.getByPhoneNumber(number);
    }

    @Override
    public PhoneNumber getByUserId(int id) {
        return repository.getByUserId(id);
    }
}
