package com.example.forumproject.services;

import com.example.forumproject.exceptions.DuplicateEntityException;
import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.exceptions.UnauthorizedOperationException;
import com.example.forumproject.models.Category;
import com.example.forumproject.models.Role;
import com.example.forumproject.models.User;
import com.example.forumproject.repositories.contracts.CategoryRepository;
import com.example.forumproject.services.contracts.CategoryService;
import com.example.forumproject.utils.ValidationHelpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.forumproject.utils.ValidationHelpers.*;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository repository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Category> getAll() {
        return repository.getAll();
    }

    @Override
    public Category getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Category getByName(String name) {
        return repository.getByName(name);
    }

    @Override
    public void create(Category category, User user) {
        validateUserIsAdmin(user);
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        boolean duplicateExists = true;
        try {
            repository.getByName(category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }
        repository.create(category);
    }

    @Override
    public void update(Category category, User user) {
        validateUserIsAdmin(user);
        //test comment - REMOVE

        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        boolean duplicateExists = true;
        try {
            repository.getByName(category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }
        repository.update(category);
    }

    @Override
    public void delete(int id, User user) {
        Category category = getById(id);
        validateUserIsAdmin(user);
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        repository.delete(category);
    }

    @Override
    public int getPostsCountInCategory(int categoryId) {
        return repository.getPostsCountInCategory(categoryId);
    }
}
