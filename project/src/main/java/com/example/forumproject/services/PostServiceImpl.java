package com.example.forumproject.services;

import com.example.forumproject.exceptions.EntityNotFoundException;
import com.example.forumproject.models.*;
import com.example.forumproject.repositories.contracts.CategoryRepository;
import com.example.forumproject.repositories.contracts.CommentRepository;
import com.example.forumproject.repositories.contracts.PostRepository;
import com.example.forumproject.repositories.contracts.TagRepository;
import com.example.forumproject.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.example.forumproject.utils.ValidationHelpers.*;

@Service
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final TagRepository tagRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, CommentRepository commentRepository, TagRepository tagRepository, CategoryRepository categoryRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.tagRepository = tagRepository;

        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Post> getAll() {
       return postRepository.getAll();
    }

    @Override
    public List<Post> search(Optional<String> search) {
        return postRepository.search(search);
    }

    @Override
    public Post getById(int id) {
        return postRepository.getById(id);
    }

    @Override
    public List<Post> getMostCommentedPosts() {
        return postRepository.getMostCommentedPosts();
    }

    @Override
    public List<Post> getTenMostRecent() {
        return postRepository.getTenMostRecent();
    }

    @Override
    public List<Post> getPostsByCategory(int id) {
        Category category = categoryRepository.getById(id);
        return postRepository.getPostsByCategory(id);
    }

    @Override
    public List<Post> filter(Optional<String> title, Optional<String> username, Optional<String> content, Optional<String> tagName,
                             Optional<String> sort) {
        return postRepository.filter(title, username, content, tagName, sort);
    }

    @Override
    public void create(Post post) {
        User user = post.getUser();
        validateUserIsDeleted(user);
        validateUserIsBlocked(user);
        postRepository.create(post);
    }

    @Override
    public void createTagInPost(Tag tag, User user, int postId) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        Post post = getById(postId);
        validateUserIsAdminOrPostCreator(user, post);
        Tag tagFromDatabase = new Tag();
        try {
            tagFromDatabase = tagRepository.getByName(tag.getTagName());
            isTagAlreadyInPost(post, tagFromDatabase);
        } catch (EntityNotFoundException e) {
            tagFromDatabase.setTagName(tag.getTagName());
            tagRepository.create(tagFromDatabase);
        }
        post.getTags().add(tagFromDatabase);
        postRepository.update(post);
    }

    @Override
    public Post votePost(User user, int postId) {
        validateUserIsDeleted(user);
        validateUserIsBlocked(user);
        Post post = getById(postId);
        boolean voted = post.getVotes().contains(user);
        if (voted) {
            post.getVotes().remove(user);
        } else {
            post.getVotes().add(user);
        }
        postRepository.update(post);
        return post;
    }


    @Override
    public void update(Post post, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsAdminOrPostCreator(user, post);
        postRepository.update(post);
    }

    @Override
    public Post updatePostCategory(Category newPostCategory, int id, User userToUpdateCategory) {
        Post post = getById(id);
        validateUserIsDeleted(userToUpdateCategory);
        validateUserIsBlocked(userToUpdateCategory);
        validateUserIsAdmin(userToUpdateCategory);
        Category existingCategory = categoryRepository.getByName(newPostCategory.getName());
        post.setCategory(existingCategory);
        postRepository.update(post);
        return post;
    }


    @Override
    public void delete(int postId, User user) {
        Post post = getById(postId);
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsAdminOrPostCreator(user, post);
        post.getVotes().clear();
        post.getTags().clear();
        post.getComments().forEach(commentRepository::deleteComment);
        postRepository.delete(post);
    }

    @Override
    public void deleteTagInPost(Tag tag, User user, int postId) {
        Post post = getById(postId);
        Tag existingTag = tagRepository.getByName(tag.getTagName());
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsAdminOrPostCreator(user, post);
        validatePostContainsTag(post, existingTag);
        post.getTags().remove(existingTag);
        postRepository.update(post);
    }

    @Override
    public List<Post> getRelatedPosts(List<String> tags, int id) {
        return postRepository.getRelatedPosts(tags, id);
    }
}
