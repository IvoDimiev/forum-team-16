package com.example.forumproject.services.contracts;

import com.example.forumproject.models.Image;

public interface ImageService {
    Image getImage(int userId);

    void saveImage(int userId, byte[] image);

    void update(Image image);
}
