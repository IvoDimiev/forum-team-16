# Forum - Team 16

This is Sofi and Ivo's Forum project.

## Database scheme:


## Project description
Soft skills forum system, where the users can create posts, add comments, and upvote the things that they like the most. </br>
Anonymous users could read the topics, but they can not post comments or new posts. If they want to be able to post, they </br>
can register a new account and log in into it. There are admins in the forum that make sure there is no spam and everything is kept clean. </br>

## Technologies used:
- JDK 11
- Spring Boot framework
- MariaDB
- Hibernate
- HTML and CSS
- Javascript

## How to build the project:
First, you need to download the project folder. </br>
You also need a MariaDB database which you can create and fill using the provided scripts.</br>
Then you should edit the settings in the application.properties file in \forum-project\src\main\resources. </br>
After completing the previous steps, you can run the ForumProjectApplication.class </br>

## Swagger Documentation
We have also provided a Swagger API Documentation that is accessible at http://localhost:8080/swagger-ui/index.html

